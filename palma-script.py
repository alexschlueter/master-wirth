import os
import sys
import shutil

import numpy as np

# from loss import LeastSquaresLoss
# from adcg import run_adcg
from models.dynamic import DynamicSuperresModel
from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel
from discretization import *
from utils import *
from solvers import *

# discstr = sys.argv[1]
jobid = sys.argv[1]
# print("I am job #{} for {}".format(jobid, discstr))
print("I am job #{}".format(jobid))
discstr = "dimreduc"

max_adcg_iters = 100 # cf Alberti

disc_single = Discretization(
    tau = 1,
    K = 1,
    num_dirs = 0,
    num_rads = 0,
    num_extra_ns = 0,
    grid_size_nu = 100,
    grid_size_mu = 0,
    adaptive_grids=True
)

disc = Discretization(
    tau = 1,
    K = 1,
    num_dirs = 30,
    num_rads = 100,
    num_extra_ns = 5,
    grid_size_nu = 100,
    grid_size_mu = 100,
    adaptive_grids=True
)

# folder = "/scratch/tmp/a_schl57/gt_sep0.03-0.06_2parts/"
folder = "../"
gt_file = folder+"gt_{}.npy".format(jobid)
_, actual_sources, weights = np.load(gt_file, allow_pickle=True)

# model_static = FourierNDModel([np.arange(-2, 3), np.arange(-2, 3)], [1, 1])
# model_static = FourierNDModel([np.arange(-10, 11), np.arange(-10, 11)], [1, 1])
fc = 3
model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])
# model_static = IdentityModel([1, 1], 2*[p.grid_size_nu])

model = DynamicSuperresModel(model_static, 1, disc.tau, disc.K)
target = model.apply(actual_sources.transpose(), weights)
l1_target = np.linalg.norm(weights, ord=1)

if discstr == "adcg":
    loss = LeastSquaresLoss()
    def cb_fun(old_support, cb_support, cb_weights, output, old_obj_val):
        new_obj_val, _ = loss.loss_and_grad(output-target)
        return old_obj_val - new_obj_val < 1e-4 # cf. Alberti

    supres, wres = run_adcg(model, loss, target, l1_target, cb_fun, max_adcg_iters)
    announce_save("ADCG res", "res_{}".format(jobid), (supres, wres))

else:
    print("Stage 1: Main objective")
    Msys, y = disc.build_main_objective(model_static, target)

    # solver = L1SeparableConeSolver(disc, Msys, y, 5.0)
    solver = SeparableConeSolver(disc, Msys, y, l1_target, nonneg=False)
    # solver = CVXQuadraticProgramSolver(disc, Msys, y, l1_target, sparse=True, kktsolver=True)

    print("Start opt...")
    start = time.time()
    mu, nu = solver.solve()
    end = time.time()
    print(end-start)
    print("Finished opt...")

    announce_save("mu, nu", "res_{}".format(jobid), (mu, nu))