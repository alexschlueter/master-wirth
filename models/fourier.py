import itertools
import cmath

import numpy as np
from numba import jit

from .abstract import ForwardModel

@jit(nopython=True)
def _apply_single_source_fourier(source, freq_grid, param_max):
    complex_vals = np.exp(-2j*np.pi*(freq_grid @ (source/param_max)))
    return np.hstack((np.real(complex_vals), np.imag(complex_vals)))

class Fourier1DModel(ForwardModel):
    def __init__(self, freq_max, x_max, n_approx=None):
        self.param_dim = 1
        if n_approx is None:
            n_approx = 10*x_max+1
        self.freq_max = freq_max
        self.freqs = np.arange(-self.freq_max,self.freq_max+1)
        self.x_max = x_max
        self.approx_grid = np.linspace(0, x_max, n_approx)

    def apply_single_source(self, source):
        complex_vals = np.exp(-2j*np.pi * self.freqs/self.x_max * source[0])
        return np.hstack((np.real(complex_vals), np.imag(complex_vals)))

    def jac_meas_fun(self, source):
        fac = -2j*np.pi * self.freqs/self.x_max
        complex_grad = fac * np.exp(fac * source[0])
        return np.hstack((np.real(complex_grad), np.imag(complex_grad))).transpose()

    def get_param_bounds(self):
        return np.array([[0], [self.x_max]])
       
    def get_starting_point(self, loss_grad):
        # idx = np.argmin([np.dot(loss_grad, self.apply_single_source([pnt])) for pnt in self.approx_grid])
        # return [self.approx_grid[idx]]
        return self.get_starting_point_from_grid(loss_grad)

    def get_param_grids(self):
        return [self.approx_grid]

    def meas_size(self):
        return 2*len(self.freqs)

class FourierNDModel(ForwardModel):
    def __init__(self, freqs, param_max, n_approx=None):
        assert len(freqs) == len(param_max)
        self.param_dim = len(freqs)
        self.freqs = np.asarray(freqs)
        self.param_max = np.asarray(param_max)
        if n_approx is None:
           n_approx = 10*self.param_max + 1         #  Alberti: 10*f_c+1
        assert len(n_approx) == len(param_max)
        self.n_approx = n_approx
        self.freq_grid = np.array(list(itertools.product(*self.freqs)), dtype=np.float64)

    def apply_single_source(self, source):
        # complex_vals = [np.exp(-2j*np.pi*np.dot(source/self.param_max, np.flipud(k)))
        #                 for k in itertools.product(*np.flipud(self.freqs))]

        # complex_vals = [cmath.exp(-2j*np.pi*np.dot(source/self.param_max, k))
        #                 for k in itertools.product(*self.freqs)]
        # complex_vals = np.exp(-2j*np.pi*(self.freq_grid@(source/self.param_max)))
        # return np.hstack((np.real(complex_vals), np.imag(complex_vals)))

        return _apply_single_source_fourier(source, self.freq_grid, self.param_max)

    def jac_meas_fun(self, source):
        # complex_grad = [-2j*np.pi*np.flipud(k)/self.param_max*np.exp(-2j*np.pi*np.dot(source/self.param_max, np.flipud(k)))
        #                 for k in itertools.product(*np.flipud(self.freqs))]
        # return np.vstack((np.real(complex_grad), np.imag(complex_grad)))
        complex_grad = [-2j*np.pi*np.asarray(k)/self.param_max*np.exp(-2j*np.pi*np.dot(source/self.param_max, k))
                        for k in itertools.product(*self.freqs)]
        return np.vstack((np.real(complex_grad), np.imag(complex_grad)))

    def get_param_bounds(self):
        return np.array([np.zeros(self.param_dim), self.param_max])

    def get_param_grids(self):
        return [np.linspace(0, pmax, n) for pmax, n in zip(self.param_max, self.n_approx)]
       
    def get_starting_point(self, loss_grad):
        return self.get_starting_point_from_grid(loss_grad)

    def meas_size(self):
        return 2*len(self.freq_grid)