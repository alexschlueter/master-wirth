import itertools
import random
import math

import numpy as np
from numba import jit

from .abstract import ForwardModel

@jit(nopython=True)
def _apply_single_source_radon(source, mesh, sigma):
        return np.exp(-(mesh[0]-mesh[1]@source)**2/2/sigma**2)

# !!!!!!!!!!!!!!!!!!!!!! angle < pi/2 ?????????????????????????
# radius sqrt(29)
class Radon2DModel(ForwardModel):
    def __init__(self, n_dirs, n_radius, n_approx=None, sigma=1.0):
        self.param_dim = 2
        self.n_dirs = n_dirs
        self.n_radius = n_radius
        self.sigma = sigma
        self.rad_grid = np.linspace(0, np.sqrt(2), self.n_radius)
        self.dir_grid = np.array([(np.cos(angle), np.sin(angle)) for angle in np.linspace(0, np.pi, n_dirs)])
        # self.rad_grid = np.linspace(-np.sqrt(2), np.sqrt(2), self.n_radius)
        # self.dir_grid = np.array([(np.cos(angle), np.sin(angle)) for angle in np.linspace(0, 2*np.pi, n_dirs)])
        self.mesh = tuple(map(np.array, zip(*itertools.product(self.rad_grid, self.dir_grid))))
        if n_approx is None:
            n_approx = 11
        self.n_approx = n_approx

    def point_spread_fun(self, x):
        return np.exp(-x**2/2/self.sigma**2)

    def point_spread_fun_deriv(self, x):
        return -x/self.sigma**2 * np.exp(-x**2/2/self.sigma**2)
    
    def apply_single_source(self, source):
        # return np.array([self.point_spread_fun(rad-np.dot(source, direc))
        #                  for rad, direc in itertools.product(self.rad_grid, self.dir_grid)])
        return _apply_single_source_radon(source, self.mesh, self.sigma)

    def jac_meas_fun(self, source):
        return np.array([-direc*self.point_spread_fun_deriv(rad-np.dot(source, direc))
                         for rad, direc in itertools.product(self.rad_grid, self.dir_grid)])

    @staticmethod
    def get_param_bounds():
        return np.array([[0.0, 0.0], [1.0, 1.0]])

    def get_param_grids(self):
        return [np.linspace(0, 1, self.n_approx)]*2
       
    def get_starting_point(self, loss_grad):
        return self.get_starting_point_from_grid(loss_grad)

# https://stackoverflow.com/a/26127012
def fibonacci_sphere(samples=1, randomize=False):
    rnd = 1.
    if randomize:
        rnd = random.random() * samples

    points = []
    offset = 2./samples
    increment = math.pi * (3. - math.sqrt(5.));

    for i in range(samples):
        y = ((i * offset) - 1) + (offset / 2);
        r = math.sqrt(1 - pow(y,2))

        phi = ((i + rnd) % samples) * increment

        x = math.cos(phi) * r
        z = math.sin(phi) * r

        points.append([x,y,z])

    return points

class RadonNDModel(ForwardModel):
    def __init__(self, n_dirs, n_radius, n_approx=None, sigma=1.0):
        self.param_dim = 2
        self.n_dirs = n_dirs
        self.n_radius = n_radius
        self.sigma = sigma
        self.rad_grid = np.linspace(0, np.sqrt(2), self.n_radius)
        self.dir_grid = np.array([(np.cos(angle), np.sin(angle)) for angle in np.linspace(0, np.pi/2, n_dirs)])
        # self.rad_grid = np.linspace(-np.sqrt(2), np.sqrt(2), self.n_radius)
        # self.dir_grid = np.array([(np.cos(angle), np.sin(angle)) for angle in np.linspace(0, 2*np.pi, n_dirs)])
        self.mesh = tuple(map(np.array, zip(*itertools.product(self.rad_grid, self.dir_grid))))
        if n_approx is None:
            n_approx = 11
        self.n_approx = n_approx

    def point_spread_fun(self, x):
        return np.exp(-x**2/2/self.sigma**2)

    def point_spread_fun_deriv(self, x):
        return -x/self.sigma**2 * np.exp(-x**2/2/self.sigma**2)
    
    def apply_single_source(self, source):
        # return np.array([self.point_spread_fun(rad-np.dot(source, direc))
        #                  for rad, direc in itertools.product(self.rad_grid, self.dir_grid)])
        return _apply_single_source_radon(source, self.mesh, self.sigma)

    def jac_meas_fun(self, source):
        return np.array([-direc*self.point_spread_fun_deriv(rad-np.dot(source, direc))
                         for rad, direc in itertools.product(self.rad_grid, self.dir_grid)])

    @staticmethod
    def get_param_bounds():
        return np.array([[0.0, 0.0], [1.0, 1.0]])

    def get_param_grids(self):
        return [np.linspace(0, 1, self.n_approx)]*2
       
    def get_starting_point(self, loss_grad):
        return self.get_starting_point_from_grid(loss_grad)