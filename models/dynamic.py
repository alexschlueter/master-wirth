import numpy as np

from .abstract import ForwardModel

class DynamicSuperresModel(ForwardModel):
    def __init__(self, static_model, v_max, tau, K, num_v=20):
        self.param_dim = 2*static_model.param_dim
        self.static_model = static_model
        self.v_max = v_max
        self.times = np.linspace(-tau*K, tau*K, 2*K+1)
        self.num_v = num_v

    def apply_single_source(self, source):
        return np.hstack([self.static_model.apply_single_source(source[:self.static_model.param_dim]
                                                              + t*source[self.static_model.param_dim:])
                          for t in self.times])

    def jac_meas_fun(self, source):
        dx = np.vstack([self.static_model.jac_meas_fun(source[:self.static_model.param_dim]
                                                     + t*source[self.static_model.param_dim:])
                      for t in self.times])
        dv = np.vstack([t * self.static_model.jac_meas_fun(source[:self.static_model.param_dim]
                                                         + t*source[self.static_model.param_dim:])
                      for t in self.times])
        return np.hstack([dx, dv])

    def get_param_bounds(self):
        stat_bnds = self.static_model.get_param_bounds()
        v_bnds = [np.full(self.static_model.param_dim, -self.v_max),
                    np.full(self.static_model.param_dim, self.v_max)]
        return np.hstack([stat_bnds, v_bnds])

    def get_param_grids(self):
        stat_grids = self.static_model.get_param_grids()
        v_grids = [np.linspace(-self.v_max, self.v_max, self.num_v)]*self.static_model.param_dim
        return stat_grids + v_grids
       
    def get_starting_point(self, loss_grad):
        return self.get_starting_point_from_grid(loss_grad)

    def meas_size(self):
        return len(self.times) * self.static_model.meas_size()