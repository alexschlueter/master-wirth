from abc import ABC, abstractmethod
import itertools

import numpy as np

class ForwardModel(ABC):
    @abstractmethod
    def apply_single_source(self, source):
        pass
    
    @abstractmethod
    def jac_meas_fun(self, support):
        pass

    @abstractmethod
    def get_param_bounds(self):
        pass

    @abstractmethod
    def get_starting_point(self, loss_grad):
        pass

    @abstractmethod
    def meas_size(self):
        pass

    def apply(self, support, weights):
        return np.sum([weight*self.apply_single_source(source)
                       for weight, source in zip(weights, support.transpose())], axis=0)

    def get_starting_point_from_grid(self, loss_grad):
        param_grids = self.get_param_grids()
        flat_idx = np.argmin([np.dot(loss_grad, self.apply_single_source(np.array(pnt)))
                              for pnt in itertools.product(*param_grids)])
        idx = np.unravel_index(flat_idx, [len(g) for g in param_grids])
        return [p_grid[i] for p_grid, i in zip(param_grids, idx)]
