import itertools

import numpy as np

from .abstract import ForwardModel

class IdentityModel(ForwardModel):
    def __init__(self, param_max, n_approx=None):
        self.param_dim = len(param_max)
        self.param_max = np.asarray(param_max)
        if n_approx is None:
           n_approx = 10*self.param_max + 1         #  Alberti: 10*f_c+1
        assert len(n_approx) == len(param_max)
        self.n_approx = n_approx

    def apply_single_source(self, source):
        pgs = self.get_param_grids()
        idx = 0
        fac = 1
        for i in range(len(source)-1, -1, -1):
            pos = np.searchsorted(pgs[i], source[i])
            if pos == 0 or pos >= len(pgs[i]):
                return np.zeros(self.meas_size())
            idx += pos*fac
            fac *= self.n_approx[i]
        res = np.zeros(self.meas_size())
        res[idx] = 1
        return res

    def jac_meas_fun(self, source):
        return np.eye(self.param_dim)

    def get_param_bounds(self):
        return np.array([np.zeros(self.param_dim), self.param_max])

    def get_param_grids(self):
        return [np.linspace(0, pmax, n) for pmax, n in zip(self.param_max, self.n_approx)]
       
    def get_starting_point(self, loss_grad):
        return self.get_starting_point_from_grid(loss_grad) # could use -loss_grad culled to domain?

    def meas_size(self):
        return np.prod(self.n_approx)