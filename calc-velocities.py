import itertools
import time

import cvxopt
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as spla

import l1regls
from discretization import *

disc = Discretization(
    tau = 1,
    K = 1,
    # num_dirs = 3,
    # num_rads = 3,
    # grid_size_nu = 3,
    # grid_size_mu = 3,
    # num_extra_ns = 3,

    # num_extra_ns = 11,
    # num_rads = 11,

    # grid_size_nu = 60,
    # grid_size_mu = 60,

    # num_dirs = 17,
    # num_rads = 17,
    # num_extra_ns = 17,
    # grid_size_nu = 20,
    # grid_size_mu = 20,
    num_dirs = 30,
    num_rads = 30,
    num_extra_ns = 30,
    grid_size_nu = 100,
    grid_size_mu = 100,
    # num_extra_ns = 30,
    # # grid_size_nu = 640,
    # # grid_size_mu = 640,
    # grid_size_nu = 200,
    # grid_size_mu = 200,

    # grid_size_nu = 17,
    # grid_size_mu = 17,

    # num_dirs = 30,
    # num_rads = 30,
    # num_extra_ns = 30,
    # grid_size_nu = 60,
    # grid_size_mu = 60,
)

mu, nu = np.load("data/res_20190429-160522.npy")
Vmat, b = disc.build_vel_objective(mu)

# Vres = np.linalg.lstsq(Vmat, b)[0]
# Vres = sp.linalg.lsmr(Vmat, b, damp=1)[0]
# Vres = l1regls.l1regls(cvxopt.matrix(Vmat), cvxopt.matrix(b))
Vres = l1regls.l1regls_mosek2(cvxopt.matrix(Vmat), cvxopt.matrix(b))
fname = "data/resV_"+time.strftime("%Y%m%d-%H%M%S")
print("Saving velocity result to {}.npy".format(fname)) 
np.save(fname, Vres)