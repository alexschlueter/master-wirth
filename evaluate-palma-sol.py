import matplotlib.pyplot as plt
import numpy as np

from discretization import *
from utils import *

# folder = "/scratch/tmp/a_schl57/gt_sep0.03-0.06_2parts/"
folder = "../"
# resfolder = "disc3rad100_fc3"
K = 1
tau = 1
times = np.linspace(-tau*K, tau*K, 2*K+1)

eval_res = []

total_tp = 0
total_pos = 0
total_srces = 0
for i in range(1,1001):
    print("eval it {}".format(i))
    gt_file = folder+"gt_{}.npy".format(i)
    _, actual_sources, weights = np.load(gt_file, allow_pickle=True)

    # mu, nu = np.load("{}{}/res_{}_{}.npy".format(folder, resfolder, discstr, i), allow_pickle=True)
    mu, nu = np.load("res_{}.npy".format(i), allow_pickle=True)
    # time_max = disc.K*disc.tau

    # los = list_of_sources(nu[-num_times_steps+ti])
    # los = list_of_sources(nu[1])
    # if discstr in ["comb", "single"]:
    #     los = list_of_sources(nu[0])
    # else:
    #     los = list_of_sources(nu[-2])
#     los = list_of_sources(nu[0 if len(nu)==1 else -2], zero_threshold=1e-2)
        # sources_for_time = actual_sources[:,:2]
    dyn_sep = min_dyn_sep(actual_sources, K, tau)
    seps = []
    true_pos = []
    detec_srcs = []
    for ti, t in enumerate(times):
        los = list_of_sources(nu[ti-len(times)], zero_threshold=1e-1)
        sources_for_time = actual_sources[:,:2]+t*actual_sources[:,2:]
        sep = min_torus_sep(sources_for_time)
        tp = match_sources(sources_for_time, los)
        true_pos.append(tp)
        seps.append(sep)
        detec_srcs.append(los)
        if t == 0:
            total_tp += tp
            total_pos += len(los)
            total_srces += len(sources_for_time)
    eval_res.append({
        "actual_sources": actual_sources,
        "weights": weights,
        "dyn_sep": dyn_sep,
        "seps": seps, 
        "true_pos": true_pos,
        "detec_srcs": detec_srcs
    })

precision = total_tp/total_pos
recall = total_tp/total_srces
f1 = 2*precision*recall / (precision + recall)
print("total_tp {} total_pos {} total_srces {} prec {} rec {} f1 {}"
        .format(total_tp, total_pos, total_srces, precision, recall, f1))
announce_save("eval res", "eval1e-1", eval_res, ".", with_time=False)


# plt.hist(seps)
# plt.show()
