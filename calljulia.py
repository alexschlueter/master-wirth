import julia
j = julia.Julia()
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/SparseInverseProblemsMod.jl")
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/abstractTypes.jl")
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/lsLoss.jl")
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/BoxConstrainedDifferentiableModel.jl")
# j.include("../dynamic_spike_super_resolution/models/SuperRes.jl")
# j.include("../dynamic_spike_super_resolution/models/DynamicSuperRes.jl")
# j.include("../dynamic_spike_super_resolution/models/Fourier1d.jl")
# j.include("../dynamic_spike_super_resolution/models/Fourier2d.jl")


# # #jmod = j.eval("Fourier1d(20, 1)")
# jmod = j.eval("Fourier2d(1, 2, ones(11, 13), 11, 21)")
# jloss = j.eval("LSLoss()")
# # jsfd = j.eval("solveFiniteDimProblem")

j.eval("push!(LOAD_PATH, \"./dynamic_spike_super_resolution/SparseInverseProblems/src\")")
j.eval("using SparseInverseProblemsMod.Util")
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/util/ip.jl")
# j.include("../dynamic_spike_super_resolution/SparseInverseProblems/src/util/ip_lasso.jl")
jnnlasso = j.eval("nnlasso")
