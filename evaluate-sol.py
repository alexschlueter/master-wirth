import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage
from sklearn.neighbors import NearestNeighbors

from discretization import *

zero_threshold = 1e-3

disc = Discretization(
    tau = 1,
    K = 1,
    # num_dirs = 3,
    # num_rads = 3,
    # grid_size_nu = 3,
    # grid_size_mu = 3,
    # num_extra_ns = 3,

    # num_extra_ns = 11,
    # num_rads = 11,

    # grid_size_nu = 60,
    # grid_size_mu = 60,

    # num_dirs = 17,
    # num_rads = 17,
    # num_extra_ns = 17,
    # grid_size_nu = 20,
    # grid_size_mu = 20,

    # grid_size_nu = 17,
    # grid_size_mu = 17,

    num_dirs = 30,
    num_rads = 30,
    num_extra_ns = 30,
    grid_size_nu = 100,
    grid_size_mu = 100,
)
disc = Discretization(
    tau = 1,
    K = 0,
    # num_dirs = 3,
    # num_rads = 3,
    # grid_size_nu = 3,
    # grid_size_mu = 3,
    # num_extra_ns = 3,

    # num_extra_ns = 11,
    # num_rads = 11,

    # grid_size_nu = 60,
    # grid_size_mu = 60,

    # num_dirs = 17,
    # num_rads = 17,
    # num_extra_ns = 17,
    # grid_size_nu = 20,
    # grid_size_mu = 20,

    # grid_size_nu = 17,
    # grid_size_mu = 17,

    # num_dirs = 30,
    # num_rads = 30,
    num_dirs = 0,
    num_rads = 0,
    # num_extra_ns = 30,
    num_extra_ns = 0,
    grid_size_nu = 100,
    grid_size_mu = 0,
    # grid_size_mu = 0,
    adaptive_grids=True
)
mu, nu = np.load("/media/alex/YUGE/master-wirth/data/res_20190923-002215.npy", allow_pickle=True)
_, actual_sources, weights = np.load("/media/alex/YUGE/master-wirth/data/gt_20190923-002212.npy", allow_pickle=True)
actual_sources = actual_sources.transpose()
print(actual_sources)

time_max = disc.K*disc.tau
# left = -1/(2*time_max)
# right = max(1, 1/(2*time_max))

def list_of_sources(img):
    img[img<zero_threshold] = 0
    labelled, num_sources = ndimage.label(img, structure=np.ones((3,3)))
    com = np.array(ndimage.center_of_mass(img, labelled, 1+np.arange(num_sources)))
    com /= disc.grid_size_nu-1
    # com *= right-left
    # com += left
    # print(com)
    return com
    # g = np.empty((disc.grid_size_nu, disc.grid_size_nu), dtype=object)
    # g[:] = disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2)
    # # print(disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2))
    # return ndimage.interpolation.map_coordinates(disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2), com.transpose())

num_times_steps = 2*disc.K+1
for ti, t in enumerate(disc.times):
    los = list_of_sources(nu[-num_times_steps+ti])
    # los *= np.sqrt(1+t**2)
    # print(los)
    sources_for_time = actual_sources[:,:2]+t*actual_sources[:,2:]
    print(sources_for_time)
    nbrs = NearestNeighbors().fit(sources_for_time)
    dists, inds = nbrs.kneighbors(los, n_neighbors=1)
    print(dists)
    # print(inds)
    print("ti={}, detected srcs={}".format(ti, len(los)))
    plt.imshow(nu[-num_times_steps+ti].transpose(), interpolation="none", origin="lower")
    plt.figure()
    plt.scatter(*zip(*los*disc.grid_size_nu), c="red")
    plt.scatter(*zip(*sources_for_time*disc.grid_size_nu), c="green", marker="x")
    plt.show()
