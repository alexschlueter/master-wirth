14.10.

- disc mit extra_ns dauert sehr schnell sehr lange (schon n=5)
- muss sehr viele sims laufen lassen, um trotz binning eine ordentliche Auflösung in der sep-Achse zu bekommen

- Plot crr gegen dyn_sep: 
    - Für kleine seps (bis ca. 0.04) ist single besser
    - ab 0.04: leichte Tendenz dyn besser
- Plot F1 gegen dyn_sep:
    - single > discn0 > discn5
    - liegt an sehr schlechter precision
    - Warum?????????????

- Alberti sogar static succesful wenn nur *ein* Zeitschritt
korrekt rekonstruiert

- RADS viel zu große range, verschwendung von vars!!!
Q_n^1 mu(th) 
th_2>0:
n_1, n_2>0
n_1*y+n_2*w in [0, n_1*sum(th)]
conv{0, n_1*sum(th), (n_1+n_2/delta)*sum(th)/2, (n_1-n_2/delta)*sum(th)/2}

Rth nu(n)
n_1-vscal>=0
conv{0, th1*n1, th2*n1, sum(th)*n1}

TODO:
- spy rad mat
disc kleine zahlen

Thesis