
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np

from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel