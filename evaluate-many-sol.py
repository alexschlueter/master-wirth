import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from datetime import datetime
from bisect import bisect_left, bisect_right

from discretization import *
from utils import *

disc = Discretization(
    tau = 1,
    K = 1,
    # num_dirs = 3,
    # num_rads = 3,
    # grid_size_nu = 3,
    # grid_size_mu = 3,
    # num_extra_ns = 3,

    # num_extra_ns = 11,
    # num_rads = 11,

    # grid_size_nu = 60,
    # grid_size_mu = 60,

    # num_dirs = 17,
    # num_rads = 17,
    # num_extra_ns = 17,
    # grid_size_nu = 20,
    # grid_size_mu = 20,

    # grid_size_nu = 17,
    # grid_size_mu = 17,

    num_dirs = 30,
    num_rads = 30,
    num_extra_ns = 30,
    grid_size_nu = 100,
    grid_size_mu = 100,
)

discs = ["full", "comb", "three", "single"]
files = dict()
# paths = dict()
    
for discstr in discs:
    files[discstr] = get_timed_files("data-home/", "res_"+discstr)
files["gt"] = get_timed_files("data-home/", "gt")

rads = np.linspace(0, 0.5, 1000)
rad = 0.01

seps = []

start_time = datetime(2019, 9, 23, 0, 50, 45)
for discstr in discs:
    tp = 0
    total_pos = 0
    total_srces = 0
    for gt_file, gt_date in filter(lambda gt: gt[1]>=start_time, zip(*files["gt"])):
        # discstr = "comb"

        res = load_res_for_time(files[discstr], gt_date)
        if res == None:
            break
        mu, nu = res
        _, actual_sources, weights = np.load(gt_file, allow_pickle=True)
        actual_sources = actual_sources.transpose()
        # print(actual_sources)

        # time_max = disc.K*disc.tau

        # los = list_of_sources(nu[-num_times_steps+ti])
        # los = list_of_sources(nu[1])
        if discstr in ["comb", "single"]:
            los = list_of_sources(nu[0])
        else:
            los = list_of_sources(nu[-2])
        # los *= np.sqrt(1+t**2)
        # print(los)
        # sources_for_time = actual_sources[:,:2]+t*actual_sources[:,2:]
        sources_for_time = actual_sources[:,:2]
        seps.append(min_torus_sep(sources_for_time))
        tp += match_sources(sources_for_time, los)
        total_pos += len(los)
        total_srces += len(sources_for_time)
        # print("ti={}, detected srcs={}".format(ti, len(los)))
        # plt.imshow(nu[-num_times_steps+ti].transpose(), interpolation="none", origin="lower")
        # plt.figure()
        # plt.scatter(*zip(*los*disc.grid_size_nu), c="red")
        # plt.scatter(*zip(*sources_for_time*disc.grid_size_nu), c="green")
        # plt.show()

    precision = tp/total_pos
    recall = tp/total_srces
    f1 = 2*precision*recall / (precision + recall)
    print(discstr, tp, total_pos, total_srces, precision, recall, f1)

# plt.hist(seps)
# plt.show()
