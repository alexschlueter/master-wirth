import sys

import numpy as np

from models.dynamic import DynamicSuperresModel
from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel
from discretization import *
from utils import *
from solvers import *

discstr = sys.argv[1]
jobid = sys.argv[2]
print("I am job #{} for {}".format(jobid, discstr))

disc_single = Discretization(
    tau = 1,
    K = 0,
    num_dirs = 0,
    num_rads = 0,
    num_extra_ns = 0,
    grid_size_nu = 100,
    grid_size_mu = 0,
    adaptive_grids=True
)
disc3rad100 = Discretization(
    tau = 1,
    K = 1,
    num_dirs = 30,
    num_rads = 100,
    num_extra_ns = 0,
    grid_size_nu = 100,
    grid_size_mu = 100,
    adaptive_grids=True
)
if discstr == "single":
    disc = disc_single
elif discstr == "disc3rad100":
    disc = disc3rad100
else:
    raise "nope discstr"

folder = "/scratch/tmp/a_schl57/gt_maxsep0.03_2parts/"
gt_file = folder+"gt_{}.npy".format(jobid)
_, actual_sources, weights = np.load(gt_file, allow_pickle=True)

# model_static = FourierNDModel([np.arange(-2, 3), np.arange(-2, 3)], [1, 1])
# model_static = FourierNDModel([np.arange(-10, 11), np.arange(-10, 11)], [1, 1])
fc = 5
model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])
# model_static = IdentityModel([1, 1], 2*[p.grid_size_nu])

model = DynamicSuperresModel(model_static, 1, disc.tau, disc.K)
target = model.apply(actual_sources.transpose(), weights)

print("Stage 1: Main objective")
Msys, y = disc.build_main_objective(model_static, target)

l1_target = np.linalg.norm(weights, ord=1)
# solver = L1SeparableConeSolver(disc, Msys, y, 5.0)
solver = SeparableConeSolver(disc, Msys, y, l1_target, nonneg=False)
# solver = CVXQuadraticProgramSolver(disc, Msys, y, l1_target, sparse=True, kktsolver=True)

print("Start opt...")
start = time.time()
mu, nu = solver.solve()
end = time.time()
print(end-start)
print("Finished opt...")

announce_save("mu, nu", "res_{}_{}".format(discstr, jobid), (mu, nu), with_time=False, folder=folder+discstr)