import cvxopt
import cvxopt.msk
import mosek
import scipy.sparse as sp

from utils import *

# cf. eq. (18) in Erling D. Andersen: "On formulating quadratic functions in optimization models" 
# URL: http://docs.mosek.com/whitepapers/qmodel.pdf
class SeparableConeSolver:
    def __init__(self, disc, Msys, y, l1_target, nonneg=True):
        self.disc = disc

        if sp.issparse(Msys):
            Msys_cvx = cvxopt.spmatrix(Msys.data, Msys.row, Msys.col)
        else:
            Msys_cvx = cvxopt.matrix(Msys)
        
        # additionally have absolute value of dofs for signed dofs
        total_dofs = disc.total_dofs if nonneg else 2*disc.total_dofs

        self.c = cvxopt.matrix(0.0, (1+total_dofs, 1))
        self.c[0] = 1

        if nonneg:
            constr_mat, constr_rhs = disc.build_nonneg_sum_constr(l1_target)
        else:
            constr_mat, constr_rhs = disc.build_l1_constr(l1_target)

        constr_mat_cvx = cvxopt.spmatrix(constr_mat.data, constr_mat.row, constr_mat.col)

        col_left = cvxopt.matrix(0.0, (constr_mat.shape[0]+1+Msys.shape[0], 1))
        col_left[constr_mat.shape[0]] = -1

        Msys_rows = cvxopt.sparse([
            [cvxopt.spmatrix(0.0, [], [], (Msys.shape[0], constr_mat.shape[1]-Msys.shape[1]))],
            [Msys_cvx]])

        self.G = cvxopt.sparse([[col_left],[constr_mat_cvx,
                                            cvxopt.matrix(0.0, (1, constr_mat.shape[1])),
                                            Msys_rows]])

        self.h = cvxopt.matrix([cvxopt.matrix(constr_rhs),
                                0,
                                cvxopt.matrix(y)])
        self.dims = {
            "l": constr_mat.shape[0],
            "q": [1+Msys.shape[0]],
            "s": []
            }

    def solve(self, backend="mosek"):
        if backend == "cvx":
            opt_res = cvxopt.solvers.conelp(self.c, self.G, self.h, dims=self.dims)
            if opt_res['status'] != 'optimal':
                print("ERROR: CVX returned non-optimal result")
            sol = np.array(opt_res['x'][-self.disc.total_dofs:]).flatten() # first entries are auxiliary vars
        elif backend == "mosek":
            solsta, primal, dual = cvxopt.msk.conelp(self.c, self.G, self.h, dims=self.dims)
            if solsta != mosek.solsta.optimal:
                print("ERROR: MOSEK returned non-optimal result!")
            sol = np.array(primal[-self.disc.total_dofs:]).flatten() # first entries are auxiliary vars
        else:
            raise ValueError("Unknown backend: {}".format(backend))
        return self.disc.unpack_sol_vec(sol)

        