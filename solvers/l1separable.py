import cvxopt
import cvxopt.msk
import mosek

from utils import *

# cf. eq. (18) in Erling D. Andersen: "On formulating quadratic functions in optimization models" 
# URL: http://docs.mosek.com/whitepapers/qmodel.pdf
class L1SeparableConeSolver:
    def __init__(self, Msys, y, l2_target, disc):
        print(Msys.getformat())
        print(len(Msys.data), len(Msys.row), len(Msys.col))
        self.disc = disc
        Msys_cvx = cvxopt.spmatrix(Msys.data, Msys.row, Msys.col, size=Msys.shape)
        
        self.c = cvxopt.matrix(0.0, (2*Msys.shape[1], 1))
        self.c[Msys.shape[1]:] = 1.0

        eye = cvxopt.spmatrix(1.0, range(Msys.shape[1]), range(Msys.shape[1]))
        # a = cvxopt.sparse([[cvxopt.spmatrix(0.0, [], [], (1, 2*Msys.shape[1]))]])
        # print(a.size)
        # b = cvxopt.sparse([[Msys_cvx, cvxopt.spmatrix(0.0, [], [], Msys.shape)]])
        # print(b.size)
        # print(Msys_cvx.size, cvxopt.spmatrix(0.0, [], [], Msys.shape).size)
        # self.G = cvxopt.sparse([
        #     [eye, -eye],
        #     [-eye, -eye],
        #     [cvxopt.spmatrix(0.0, [], [], (1, 2*Msys.shape[1]))],
        #     [Msys_cvx, cvxopt.spmatrix(0.0, [], [], Msys.shape)]
        # ])
        zero_row = cvxopt.spmatrix(0.0, [], [], (1, Msys.shape[1]))
        print(eye.size, zero_row.size, Msys_cvx.size, Msys.shape)
        self.G = cvxopt.sparse([
            [eye,
             -eye,
             zero_row,
             Msys_cvx],
             [-eye,
              -eye,
              zero_row,
              cvxopt.spmatrix(0.0, [], [], Msys.shape)]
        ])

        self.h = cvxopt.matrix([cvxopt.matrix(0.0, (2*Msys.shape[1], 1)),
                                l2_target,
                                # cvxopt.matrix(l2_target, (1, 1)),
                                cvxopt.matrix(y)])
        self.dims = {
            "l": 2*Msys.shape[1],
            "q": [1+Msys.shape[0]],
            "s": []
            }

    def solve(self, backend="mosek"):
        if backend == "cvx":
            opt_res = cvxopt.solvers.conelp(self.c, self.G, self.h, dims=self.dims)
            if opt_res['status'] != 'optimal':
                print("ERROR: CVX returned non-optimal result")
            sol = np.array(opt_res['x'][:self.dims["l"]//2]).flatten()
        elif backend == "mosek":
            solsta, primal, dual = cvxopt.msk.conelp(self.c, self.G, self.h, dims=self.dims)
            if solsta != mosek.solsta.optimal:
                print("ERROR: MOSEK returned non-optimal result!")
            sol = np.array(primal[:self.dims["l"]//2]).flatten()
        else:
            raise ValueError("Unknown backend: {}".format(backend))
        return self.disc.unpack_sol_vec(sol)

        