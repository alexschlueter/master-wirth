import cvxopt
import cvxopt.cholmod
from utils import *

class CVXQuadraticProgramSolver:
    def __init__(self, Msys, y, l1_target, sparse=True, kktsolver=False):
        raise "broken"
        self.kktsolver = kktsolver
        if sparse:
            Msys_cvx = cvxopt.spmatrix(Msys.data, Msys.row, Msys.col)
            self.P = Msys_cvx.T*Msys_cvx
            nonzero = len(self.P)
            percent_nz = nonzero/np.prod(self.P.size)*100
            print('P shape = {}, nonzero = {} ({:.3}%)'.format(Msys.size, nonzero, percent_nz))

            nn_sum_mat = build_nonneg_sum_constr_mat(dofs_per_var)
            self.G = cvxopt.spmatrix(nn_sum_mat.data, nn_sum_mat.row, nn_sum_mat.col)

        else:
            self.P = cvxopt.matrix((Msys.T@Msys).todense())
            nn_sum_mat = build_nonneg_sum_constr_mat(dofs_per_var)
            self.G = cvxopt.matrix(nn_sum_mat.todense())

        self.q = cvxopt.matrix(-Msys.T@y)
        self.h = cvxopt.matrix([cvxopt.matrix(0.0, (Msys.shape[1], 1)),
                                cvxopt.matrix(l1_target, (len(dofs_per_var), 1))])
                    
    # def get_sparsity_indices():
    #     print("jo")
    #     # lmat2 = cvxopt.spmatrix(1.0, obj_mat.I, obj_mat.J, obj_mat.size) # vewwy slowww.. >:(
    #     lmat = obj_mat+ineq_mat.T*ineq_mat # prepare matrix with correct sparsity pattern
    #     print("ne")
    #     # lmat2 += ineq_res
    #     # assert list(lmat2.I) == list(lmat.I) and list(lmat2.J) == list(lmat.J)
    #     sz = lmat.size[0]
    #     ssz = ineq_mat.size[0]-sz
    #     print("jo")
    #     # obj_mat_pat = cvxopt.spmatrix(0.0, lmat.I, lmat.J, lmat.size) # vewwy slowww.. >:(
    #     obj_mat_pat = cvxopt.sparse(lmat)
    #     obj_mat_pat -= lmat
    #     print("ne")
    #     obj_mat_pat += obj_mat
    #     # assert list(obj_mat_pat.I) == list(lmat.I) and list(obj_mat_pat.J) == list(lmat.J)
    #     # lmat2 = cvxopt.matrix(0.0, obj_mat.size)
    #     col_ptrs, row_ids, vals = lmat.CCS
    #     row_ids = np.array(row_ids).flatten()
    #     indices = np.empty((ssz, grid_size*grid_size), dtype=int)
    #     diag_indices = np.empty(sz, dtype=int)
    #     for v in range(ssz):
    #         for c in range(grid_size):
    #             col_start = col_ptrs[int(v*grid_size+c)]
    #             col_end = col_ptrs[int(v*grid_size+c+1)]
    #             window = row_ids[col_start:col_end]
    #             row_start = np.searchsorted(window, v*grid_size)
    #             indices[v, c*grid_size:(c+1)*grid_size] = col_start+row_start+np.arange(grid_size)
    #             diag_indices[v*grid_size+c] = col_start+row_start+c

    #     indices = indices.flatten()
    #     V_tmp = np.zeros(len(lmat))

    # def Pfunc(x, y, alpha=1.0, beta=0.0):
    #     y *= beta
    #     y += alpha*meas.T*(meas*x)

    def Fkkt(self, W):
        # global lmat
        disq = W['di']**2
        disq_np = np.array(disq).flatten()
        di2 = cvxopt.spdiag(disq)

        lmat = self.P+self.G.T*di2*self.G

        # lmat.V = obj_mat_pat.V
        # V_tmp[indices] = np.repeat(disq_np[sz:], grid_size*grid_size)
        # V_tmp[diag_indices] += disq_np[:sz]
        # lmat.V += cvxopt.matrix(V_tmp)

        # lmat += ineq_mat.T*di2*ineq_mat

        rmat = self.G.T*di2
        # cvxopt.blas.gemm(ineq_mat2, di22, rmat2, transA='T')

        # cvxopt.lapack.potrf(lmat)
        # F = cvxopt.cholmod.symbolic(lmat)
        cvxopt.cholmod.numeric(lmat, self.F)

        # cvxopt.lapack.potrf(lmat2)
        # kktm = cvxopt.sparse([[obj_mat, ineq_mat.T],
        #                       [ineq_mat, -cvxopt.spdiag(W['d']**2)])
        # cvxopt.lapack.potrf(kktm)

        def g(x, y, z):
            # res = cvxopt.matrix(0.0, (kktm.size[1], 1))
            # cvxopt.lapack.potrs(kktm, res)
            # n = x.size[0]
            # x = res[:n]
            # z = res[n:]
            # x2=+x

            x += rmat*z

            # cvxopt.blas.gemv(rmat2, z, x2, beta=1.0)
            # assert np.allclose(x, x2)

            # cvxopt.lapack.potrs(lmat, x)
            cvxopt.cholmod.solve(self.F, x)

            # cvxopt.lapack.potrs(lmat2, x2)
            # assert np.allclose(x, x2)
            # z2 = +z
            # cvxopt.blas.gemv(ineq_mat2, x2, z2, beta=-1.0)
            # z2 = cvxopt.mul(W['di'], z2)
            z[:] = cvxopt.mul(W['di'], self.G*x-z)
            # assert np.allclose(z, z2)
            # x=x2
            # z=z2

        return g

    def solve(self, *args, **kwargs):
        if self.kktsolver:
            lmat = self.P+self.G.T*self.G
            self.F = cvxopt.cholmod.symbolic(lmat)
            opt_res = cvxopt.solvers.qp(self.P, self.q, self.G, self.h, kktsolver=self.Fkkt, *args, **kwargs)
        else:
            opt_res = cvxopt.solvers.qp(self.P, self.q, self.G, self.h, *args, **kwargs)

        if opt_res['status'] != 'optimal':
            print("ERROR: CVX returned non-optimal result")
        return np.array(opt_res['x']).flatten()