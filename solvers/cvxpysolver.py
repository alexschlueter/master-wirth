import numpy as np
import cvxpy as cp

class CVXPYSolver:
    def __init__(self, Msys, y, disc):
        self.disc = disc
        self.mu = cp.Variable(disc.num_dirs*disc.gs_mu_sq)
        self.nu = cp.Variable(disc.num_total_ns*disc.gs_nu_sq)
        constraints = [cp.norm(Msys@cp.hstack((self.mu, self.nu))-y) <= 1e-12]
        obj = cp.Minimize(cp.norm(self.mu, 1))
        # obj = cp.Minimize(cp.norm(cp.hstack((self.mu, self.nu)), 1))
        self. prob = cp.Problem(obj, constraints)

    def solve(self):
        self.prob.solve(verbose=True)
        print("CVXPY status:", self.prob.status)
        print(self.nu.value)
        return self.disc.unpack_sol_vec(np.hstack((self.mu.value, self.nu.value)))