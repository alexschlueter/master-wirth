import warnings

import numpy as np
from numpy.linalg import norm

class OptimizationProblem:
    def __init__(self, d, m, f_g_h, C, h):
        self.d = d
        self.m = m
        self.f_g_h = f_g_h
        self.C = C
        self.h = h

def nnlasso(A, y, tau, x=None, l=None):
    K = A.T @ A
    b = A.T @ y
    n = K.shape[1]
    if n == 0:
        return np.empty(0)

    if x is None:
        x = tau*np.random.rand(len(b))/(len(b)*2)
    if l is None:
        l = np.ones(len(b)+1)/(2*len(b))
    
    def f_g_h(x):
        return 0.5*np.dot(x, K@x) - np.dot(x, b), K@x-b, K

    p = OptimizationProblem(n, n+1, f_g_h, np.vstack([-np.eye(n), np.ones((1, n))]), np.hstack([np.zeros(n), [tau]]))
    x, _ = primal_dual_solve(p, x, l, 10.0, 1e-12, 1e-12, 1000, 0.1, 0.4)
    x[x<1e-9] = 0
    return x


######
# Simple Primal-Dual IP for the problem
#
# min_x f(x)
#   Cx -h <= 0.0

# BV page 610
# The newton system is
# [\nabla^2 f_0, C' ]
# [-diag(\lambda) *C, -diag(C*x -h ) ]

def primal_dual_solve(p, x, lamb, mu, e_feas, e, max_iters, alpha, beta):
    assert mu > 1
    assert 0 < beta < 1
    assert p.d == len(x)
    assert p.m == len(lamb)
    assert np.all(p.C@x <= p.h)
    assert np.all(lamb > 0)
    assert e > 0 and e_feas > 0
    
    def eta_hat(x, lamb):
        return -np.dot(lamb, p.C@x-p.h)

    def compute_r_t(f, g, x, lamb, t):
        return np.hstack([g + p.C.T@lamb,
                        -lamb*(p.C@x - p.h) - (1.0/t)*np.ones(p.m)])

    for itr in range(max_iters):
        e_hat = eta_hat(x, lamb)
        t = (p.m*mu)/e_hat
        f,g,h = p.f_g_h(x)
        assert len(g) == p.d
        assert h.shape == (p.d, p.d)

        # check termination conditions... Here?
        r_old = norm(compute_r_t(f, g, x, lamb, t))
        if itr == max_iters-1:
            print(norm(g + p.C.T@lamb))
            print(e_hat)
            print()
        if norm(g + p.C.T@lamb) < e_feas and e_hat < e:
            break
        # form newton system...
        newton_system = np.block([
            [h, p.C.T],
            [-np.diag(lamb)@p.C, -np.diag(p.C@x - p.h)]
        ])
        # target
        r_dual = g + p.C.T@lamb
        r_cent = -lamb*(p.C@x - p.h) - (1.0/t)*np.ones(p.m)
        # solve
        search_direction = -np.linalg.solve(newton_system, np.hstack([r_dual, r_cent]))
        sd_x = search_direction[:p.d]
        sd_lambda = search_direction[p.d:]
        # line search
        neg_inds = np.flatnonzero(sd_lambda < 0) # mhhhh
        s_max = 1
        if len(neg_inds) > 0:
            s_max = min(1, np.min(-lamb[neg_inds]/sd_lambda[neg_inds]))
        # hardcoded in BV
        s = 0.995*s_max
        # backtracking for feasibility
        looping_index2 = 1
        while not np.all(p.C@(x + s*sd_x) <= p.h):
            s = beta*s
            if looping_index2 == 1000:
                raise Exception("infinite loop during feasibility backtracking")
            looping_index2 += 1

        # backtracking for sufficient norm decrease of residual
        looping_index = 1
        while True:
            if looping_index % 100 == 0:
                print("primal_dual_solve: backtracking iteration #{}".format(looping_index))
            looping_index += 1
            x_plus = x + s*sd_x
            lambda_plus = lamb + s*sd_lambda
            f_t, g_t, _ = p.f_g_h(x_plus)
            # compute norm of r_t
            r_t = compute_r_t(f_t, g_t, x_plus, lambda_plus, t)
            if norm(r_t) <= (1-alpha*s)*r_old:
                break
            s = beta*s

        x += s*sd_x
        lamb += s*sd_lambda

    if itr == max_iters:
        warnings.warn("primal_dual_solve: Hit max iterations in interior point method!")
    
    return x, lamb
        