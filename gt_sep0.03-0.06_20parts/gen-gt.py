import os
import shutil

import numpy as np

from utils import *

folder = "gt_sep0.03-0.06_20parts"
try:
    os.mkdir(folder)
except FileExistsError:
    pass

shutil.copy(os.path.realpath(__file__), folder+"/")

num_gts = 400
min_num_srcs = 10
max_num_srcs = 20
minsep = 0.03
maxsep = 0.06
wmin = 0.9
wmax = 1.1

num_sep_bins = 100
sep_bins = np.linspace(0, 0.08, num_sep_bins)
sep_counts = np.zeros(num_sep_bins)

for it in range(num_gts):
    print("~~~~~~~~~~~ It {} ~~~~~~~~~~~~~".format(it))
    num_sources = np.random.randint(min_num_srcs, max_num_srcs+1)
    support = np.empty((num_sources, 4))
    while True:
        for i in range(num_sources):
            while True:
                pos = np.random.rand(2)
                vel = np.random.rand(2)-0.5
                before = pos-vel
                after = pos+vel
                if np.all((0 < before) & (0 < after) & (before < 1) & (after < 1)):
                    support[i] = np.hstack((pos, vel))
                    break

        sep = min_dyn_sep(support, 1, 1)
        print(sep)
        idx = np.searchsorted(sep_bins, sep)
        if idx < num_sep_bins and sep_counts[idx] <= num_gts/num_sep_bins+1:
            sep_counts[idx] += 1
            break
        # if  minsep < sep < maxsep:
        #     break

    weights = wmin + np.random.rand(num_sources)*(wmax-wmin)
    # announce_save("generated sources", "gt_{}".format(it+1), (None, support, weights), folder=folder, with_time=False)
    print("ACCEPT!!!!!!!!!!!!", support)