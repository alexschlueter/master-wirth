import os
import shutil

import numpy as np

from utils import *

folder = "sep0-0.1"
try:
    os.mkdir(folder)
except FileExistsError:
    pass

shutil.copy(os.path.realpath(__file__), folder+"/")

num_gts = 400
min_num_srcs = 4
max_num_srcs = 20
# minsep = 0.03
# maxsep = 0.06
wmin = 0.9
wmax = 1.1

seps = np.load("distr/separations.npy")
print("Loaded distr of {} separations".format(len(seps)))

num_bins = 500
hist, bins = np.histogram(seps, num_bins, range=(0, 0.1), density=True)
min_density = np.min(hist)

# rejection sampling
for it in range(num_gts):
    print("It {}".format(it))
    while True:
        num_sources = np.random.randint(min_num_srcs, max_num_srcs+1)
        support = np.empty((num_sources, 4))
        for i in range(num_sources):
            while True:
                pos = np.random.rand(2)
                vel = np.random.rand(2)-0.5
                before = pos-vel
                after = pos+vel
                if np.all((0 < before) & (0 < after) & (before < 1) & (after < 1)):
                    support[i] = np.hstack((pos, vel))
                    break

        sep = min_dyn_sep(support, 1, 1)
        idx = np.digitize(sep, bins)-1
        if idx < len(hist):
            density = hist[idx]
            reject = np.random.rand()
            if reject < min_density/density:
                weights = wmin + np.random.rand(num_sources)*(wmax-wmin)
                announce_save("generated sources", "gt_{}".format(it+600+1), (None, support, weights), folder=folder)
                break
        #     else:
        #         print("Reject! {:.3f}>={:.3f}/{:.3f}".format(reject, min_density, density))
        # else:
        #     print("Reject! Sep {:.3f} out of range".format(sep))
