import numpy as np
import cvxopt
import nlopt
from scipy.optimize import minimize

def set_nlopt_stopping_criteria(opt_problem):
    opt_problem.set_ftol_abs(1e-6)
    opt_problem.set_xtol_rel(0.0)
    opt_problem.set_maxeval(200)

def find_new_source(forward_model, loss_grad):
    def obj_fun(test_src):
        val = np.dot(forward_model.apply_single_source(test_src), loss_grad)
        jac = loss_grad @ forward_model.jac_meas_fun(test_src)
        return val, jac
    
    bounds = forward_model.get_param_bounds().transpose()
    # opt_res = minimize(obj_fun, forward_model.get_starting_point(loss_grad), method='TNC',
    opt_res = minimize(obj_fun, forward_model.get_starting_point(loss_grad), method='L-BFGS-B',
                       jac=True, bounds=bounds)
    if not opt_res.success:
        print("Warning (find_new_source): optimization returned non-optimal result")
    return opt_res.x, opt_res.fun

def locally_improve_support(forward_model, loss, support, weights, target):
    def obj_fun(test_supp):
        test_supp = test_supp.reshape(support.shape)
        output = forward_model.apply(test_supp, weights)
        residual = output - target
        loss_val, loss_grad = loss.loss_and_grad(residual)
        jac = np.hstack(np.transpose([weight * loss_grad
                        @ forward_model.jac_meas_fun(source)
                        for weight, source in zip(weights, test_supp.transpose())]))
        return loss_val, jac

    bounds = np.repeat(forward_model.get_param_bounds().transpose(), support.shape[1], axis=0)
    opt_res = minimize(obj_fun, support.flatten(), method='L-BFGS-B',
                       jac=True, bounds=bounds)
    if not opt_res.success:
        print("Warning (locally_improve_support): optimization returned non-optimal result")
    return opt_res.x.reshape(support.shape)


# def find_new_source(forward_model, loss_grad):
#     def obj_fun(test_src, grad):
#         val = np.dot(forward_model.apply_single_source(test_src), loss_grad)
#         if grad.size > 0:
#             grad[:] = loss_grad @ forward_model.jac_meas_fun(test_src)
#         return val

#     start_pnt = forward_model.get_starting_point(loss_grad)
#     opt = nlopt.opt(nlopt.LD_MMA, len(start_pnt))
#     set_nlopt_stopping_criteria(opt)
#     lower_bounds, upper_bounds = forward_model.get_param_bounds()
#     opt.set_lower_bounds(lower_bounds)
#     opt.set_upper_bounds(upper_bounds)
#     opt.set_min_objective(obj_fun)
#     opt_res = opt.optimize(start_pnt)
#     if opt.last_optimize_result() < 0:
#         print("Warning (find_new_source): optimization returned non-optimal result")
#     return opt_res, opt.last_optimum_value()


# def locally_improve_support(forward_model, loss, support, weights, target):
#     def obj_fun(test_supp, grad):
#         test_supp = test_supp.reshape(support.shape)
#         output = forward_model.apply(test_supp, weights)
#         residual = output - target
#         loss_val, loss_grad = loss.loss_and_grad(residual)
#         if grad.size > 0:
#             grad[:] = np.hstack(np.transpose([weight * loss_grad
#                                 @ forward_model.jac_meas_fun(source)
#                                 for weight, source in zip(weights, test_supp.transpose())]))
#         return loss_val

#     opt = nlopt.opt(nlopt.LD_MMA, support.size)
#     set_nlopt_stopping_criteria(opt)
#     lower_bounds, upper_bounds = forward_model.get_param_bounds()
#     opt.set_lower_bounds(np.repeat(lower_bounds, support.shape[1]))
#     opt.set_upper_bounds(np.repeat(upper_bounds, support.shape[1]))
#     opt.set_min_objective(obj_fun)
#     opt_res = opt.optimize(support.flatten()).reshape(support.shape)
#     if opt.last_optimize_result() < 0:
#         print("Warning (locally_improve_support): optimization returned non-optimal result")
#     return opt_res


# def locally_improve_support(forward_model, loss, support, weights, target):
#     return j.eval("localDescent")(jmod, jloss, support, weights, target)


def local_update(forward_model, loss, support, target, max_mass, max_iters):
    for _ in range(max_iters):
        weights = loss.optimize_weights(forward_model, support, target, max_mass)
        print("opt_weights", weights)
        zeros = (weights == 0)
        if np.any(zeros):
            print("Removing {} zero-weight sources.".format(np.sum(zeros)))
            support = support[:, ~zeros]
            weights = weights[~zeros]
        new_support = locally_improve_support(forward_model, loss, support, weights, target)
        print("improve_supp", new_support)
        print("term", np.linalg.norm(new_support-support, np.inf))
        if (new_support.shape[1] == support.shape[1]
                and np.linalg.norm(new_support-support, np.inf) < 1e-7):
            break
        support = new_support
    weights = loss.optimize_weights(forward_model, support, target, max_mass)
    print("opt_weights_final", weights)
    zeros = (weights == 0)
    if np.any(zeros):
        print("Removing {} zero-weight sources.".format(np.sum(zeros)))
        support = support[:, ~zeros]
        weights = weights[~zeros]
    return support, weights

def run_adcg(forward_model, loss, target, max_mass, callback,
             max_iters=50,
             min_optimality_gap=1e-5,
             max_cd_iters=200):

    assert max_mass > 0
    bound = -np.inf
    support = np.empty((forward_model.param_dim, 0))
    weights = []
    output = np.zeros_like(target)
    for itr in range(max_iters):
        print("tac", itr)
        residual = output - target
        objective_value, grad = loss.loss_and_grad(residual)
        new_src, score = find_new_source(forward_model, grad)
        print("next_src", new_src, score)
        bound = max(bound, objective_value + score*max_mass - np.dot(output, grad))
        if (objective_value < bound + min_optimality_gap or score >= 0):
            return support, weights
        old_support = support
        print(support)
        print(new_src)
        support = np.hstack((support, new_src.reshape(len(new_src), 1)))
        old_weights = weights
        support, weights = local_update(forward_model, loss, support, target, max_mass, max_cd_iters)
        output = forward_model.apply(support, weights)
        if callback(old_support, support, weights, output, objective_value):
            return old_support, old_weights

    print("Warning: Hit max iterations in run_ADCG")
    return support, weights
