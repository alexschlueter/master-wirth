import itertools
import time

import cvxopt
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np

import l1regls
from models.dynamic import DynamicSuperresModel
from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel
from discretization import *
from solvers.separable import SeparableConeSolver
from solvers.quadratic import CVXQuadraticProgramSolver
from solvers.l1separable import L1SeparableConeSolver



# np.set_printoptions(threshold=np.inf)



# @profile
def main():
    disc_single = Discretization(
        tau = 1,
        K = 0,
        num_dirs = 0,
        num_rads = 0,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 0,
        adaptive_grids=True
    )

    disc3 = Discretization(
        tau = 1,
        K = 1,
        # num_dirs = 3,
        # num_rads = 3,
        # grid_size_nu = 3,
        # grid_size_mu = 3,
        # num_extra_ns = 3,

        # num_extra_ns = 11,
        # num_rads = 11,

        # grid_size_nu = 60,
        # grid_size_mu = 60,

        # num_dirs = 17,
        # num_rads = 17,
        # num_extra_ns = 17,
        # grid_size_nu = 20,
        # grid_size_mu = 20,

        # grid_size_nu = 17,
        # grid_size_mu = 17,

        num_dirs = 30,
        num_rads = 30,
        # num_extra_ns = 30,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 100,
        adaptive_grids=True
    )

    discfull = Discretization(
        tau = 1,
        K = 1,
        # num_dirs = 3,
        # num_rads = 3,
        # grid_size_nu = 3,
        # grid_size_mu = 3,
        # num_extra_ns = 3,

        # num_extra_ns = 11,
        # num_rads = 11,

        # grid_size_nu = 60,
        # grid_size_mu = 60,

        # num_dirs = 17,
        # num_rads = 17,
        # num_extra_ns = 17,
        # grid_size_nu = 20,
        # grid_size_mu = 20,

        # grid_size_nu = 17,
        # grid_size_mu = 17,

        num_dirs = 30,
        num_rads = 30,
        num_extra_ns = 30,
        grid_size_nu = 100,
        grid_size_mu = 100,
        adaptive_grids=True
    )

    num_sources = 20
    vel_std = 0.1

    for it in range(300):
        print("~~~~~~~~~~~ It {} ~~~~~~~~~~~~~".format(it))
        support = []
        while len(support) < num_sources:
            pos = np.random.rand(num_sources-len(support), 2)
            vels = np.random.normal(scale=vel_std, size=(num_sources-len(support), 2))
            for i in range(len(pos)):
                before = pos[i]-disc3.K*disc3.tau*vels[i] 
                after = pos[i]+disc3.K*disc3.tau*vels[i] 
                if np.all((0<=before) & (0<=after) & (before<=1) & (after<=1)):
                    support.append(np.hstack((pos[i], vels[i])))
                else:
                    print("Rejected source pos={}, vel={}".format(pos[i], vels[i]))
        support = np.array(support).transpose()
        # weights = 2*np.random.rand(num_sources)-1
        weights = np.random.rand(num_sources)
        announce_save("generated sources", "gt", (None, support, weights))

        # model_static = FourierNDModel([np.arange(-2, 3), np.arange(-2, 3)], [1, 1])
        # model_static = FourierNDModel([np.arange(-10, 11), np.arange(-10, 11)], [1, 1])
        fc = 5
        model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])
        # model_static = IdentityModel([1, 1], 2*[p.grid_size_nu])

        for thedisc in ["full", "comb", "single", "three"]:
            if thedisc == "single":
                model = DynamicSuperresModel(model_static, 1, disc_single.tau, disc_single.K)
                target = model.apply(support, weights)
                disc = disc_single
            else:
                model = DynamicSuperresModel(model_static, 1, disc3.tau, disc3.K)
                target = model.apply(support, weights)
                if thedisc == "comb":
                    target = target[:model_static.meas_size()]+target[model_static.meas_size():2*model_static.meas_size()] + target[2*model_static.meas_size():]
                    disc = disc_single
                elif thedisc == "three":
                    disc = disc3
                elif thedisc == "full":
                    disc = discfull

            print("Stage 1: Main objective")
            Msys, y = disc.build_main_objective(model_static, target)

            solver = L1SeparableConeSolver(Msys, y, 1.0, disc)
            # solver = SeparableConeSolver(Msys, y, weights.sum(), disc)
            # solver = CVXQuadraticProgramSolver(Msys, y, weights.sum(), disc, sparse=True, kktsolver=True)

            print("Start opt...")
            start = time.time()
            mu, nu = solver.solve()
            end = time.time()
            print(end-start)
            print("Finished opt...")
            # res[res<1e-6]=0

            announce_save("mu, nu", "res_"+thedisc, (mu, nu))
    # print(res[-(p.K+1)*p.gs_nu_sq:-p.K*p.gs_nu_sq].reshape((p.grid_size_nu, -1)))

    # print("Stage 2: Reconstruct velocities")

    # Vmat, b = disc.build_vel_objective(mu)

    # # Vres = np.linalg.lstsq(Vmat, b)[0]
    # # Vres = sp.linalg.lsmr(Vmat, b, damp=1)[0]
    # Vres = np.array(l1regls.l1regls(cvxopt.matrix(Vmat), cvxopt.matrix(b))).reshape(2, disc.grid_size_nu, -1)
    # announce_save("velocity result", "resV", Vres)

    # fig = plt.figure(figsize=(50,50))
    # for k in range(disc.num_time_steps):
    #     fig.add_subplot(1,disc.num_time_steps+2,k+1).imshow(nu[-disc.num_time_steps+k].transpose(),
    #                                                    interpolation="none", origin="lower")
    # for i in range(2):
    #     fig.add_subplot(1,disc.num_time_steps+2,disc.num_time_steps+1+i).imshow(Vres[i].transpose(),
    #                                                    interpolation="none", origin="lower")
    # fig.savefig("data/fig_"+time.strftime("%Y%m%d-%H%M%S"))

main()

