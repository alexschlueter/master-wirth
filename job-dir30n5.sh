#!/bin/bash
 
#SBATCH --job-name=master-thesis         # the name of your job
#SBATCH --output=output_%a.dat      # the file where output is written to (stdout/stderr)

#SBATCH --partition=normal          # on which partition to submit the job
#SBATCH --time=01:30:00             # the max wallclock time (time limit your job will run)

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem-per-cpu=2G

#SBATCH --array=1-1000

export PYTHONPATH="/home/a/a_schl57/code/master-wirth:$PYTHONPATH"
srun python ./palma-script.py $SLURM_ARRAY_TASK_ID