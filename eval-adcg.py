import os

import matplotlib.pyplot as plt
import numpy as np

from discretization import *
from utils import *

# folder = "/scratch/tmp/a_schl57/gt_sep0.03-0.06_2parts/"
folder = "../"
# resfolder = "disc3rad100_fc3"
K = 1
tau = 1
times = np.linspace(-tau*K, tau*K, 2*K+1)

eval_res = []

total_tp = 0
total_pos = 0
total_srces = 0
for i in range(1,1001):
    print("eval it {}".format(i))
    if not os.path.isfile("res_{}.npy".format(i)):
        eval_res.append(None)
        continue
    gt_file = folder+"gt_{}.npy".format(i)
    _, actual_sources, weights = np.load(gt_file, allow_pickle=True)

    _, sest, west = np.load("res_{}.npy".format(i), allow_pickle=True)

    thresh = west > 1e-1
    sest = sest[thresh]
    west = west[thresh]

    dyn_sep = min_dyn_sep(actual_sources, K, tau)
    seps = []
    true_pos = []
    detec_srcs = []
    for ti, t in enumerate(times):
        sources_for_time = actual_sources[:,:2]+t*actual_sources[:,2:]
        est_sources_for_time = sest[:,:2]+t*sest[:,2:]
        sep = min_torus_sep(sources_for_time)
        tp = match_sources(sources_for_time, est_sources_for_time)
        true_pos.append(tp)
        seps.append(sep)
        detec_srcs.append(sest)
        if t == 0:
            total_tp += tp
            total_pos += len(sest)
            total_srces += len(sources_for_time)
    eval_res.append({
        "actual_sources": actual_sources,
        "weights": weights,
        "sest": sest,
        "west": west,
        "dyn_sep": dyn_sep,
        "seps": seps, 
        "true_pos": true_pos,
        "detec_srcs": detec_srcs
    })

precision = total_tp/total_pos
recall = total_tp/total_srces
f1 = 2*precision*recall / (precision + recall)
print("total_tp {} total_pos {} total_srces {} prec {} rec {} f1 {}"
        .format(total_tp, total_pos, total_srces, precision, recall, f1))
announce_save("eval res", "eval_adcg_1e-1", eval_res, ".", with_time=False)


# plt.hist(seps)
# plt.show()
