import numpy as np
from scipy.optimize import linprog

import itertools

import matplotlib.pyplot as plt

from models.fourier import Fourier1DModel, FourierNDModel
from models.dynamic import DynamicSuperresModel
from optimization.nnlasso import nnlasso

disc = Discretization(
    tau = 1,
    K = 0,
    # num_dirs = 3,
    # num_rads = 3,
    # grid_size_nu = 3,
    # grid_size_mu = 3,
    # num_extra_ns = 3,

    # num_extra_ns = 11,
    # num_rads = 11,

    # grid_size_nu = 60,
    # grid_size_mu = 60,

    # num_dirs = 17,
    # num_rads = 17,
    # num_extra_ns = 17,
    # grid_size_nu = 20,
    # grid_size_mu = 20,

    # grid_size_nu = 17,
    # grid_size_mu = 17,

    num_dirs = 0,
    num_rads = 0,
    # num_extra_ns = 30,
    num_extra_ns = 0,
    grid_size_nu = 100,
    grid_size_mu = 0,
    adaptive_grids=True
)
#support = np.array([[0.5, 1], [0.3, 1], [0.6, 1.4]]).transpose()
# support = np.array([[0.5, 0.99, 0.2, 0.2], [0.3, 0.99, 0.1, -0.4], [0.6, 0.3, -0.2, 0.1]]).transpose()
# support = np.array([[0.2, 0.5]]).transpose()
#weights = [1, 2, 3]

model_static = FourierNDModel([np.arange(-10, 11), np.arange(-12, 13)], [1, 1])
# model_static = Fourier1DModel(10, 1)
model = DynamicSuperresModel(model_static, 0.7, 1, 2, 20)
# support = np.array([[0.5, 1, 0.2, 0.2], [0.3, 1, 0.1, -0.4], [0.6, 1.4, -0.2, 0.6]]).transpose()
support = np.array([[0.5, 0.2], [0.3, -0.4], [0.6, 0.6]]).transpose()
weights = np.array([1.0,2.0,3.0])
# support = np.array([[0.2, 0.4], [0.2, 1], [0.3, 1.2], [0.8, 1]]).transpose()
# weights = np.array([1.0,2.0,3.0,4.0])

# model = Fourier1DModel(20, 1, 101)
# support = np.array([[0.2, 0.3, 0.8]])
# weights = np.array([1, 2, 4])


target = model.apply(support, weights)

# grid_size = 102

# B = np.empty((target.size, grid_size))
# for j in range(grid_size):
#     complex_vals = np.exp(-2j*np.pi * model.freqs/(grid_size-1) * j)
#     B[:,j] = np.hstack((np.real(complex_vals), np.imag(complex_vals)))

param_grids = model.get_param_grids()
grid_size = np.prod([len(g) for g in param_grids])
B = np.empty((model.meas_size(), grid_size))
for j, pnt in enumerate(itertools.product(*param_grids)):
    B[:,j] = model.apply_single_source(np.array(pnt))
# c = np.ones(grid_size)
# res = linprog(c, A_eq=B, b_eq=target, bounds=(0,None), method='interior-point', options={'disp':True})
# res = linprog(c, A_eq=B, b_eq=target, bounds=(0,None), method='simplex', options={'disp':True})
res = nnlasso(B, target, weights.sum())
print(res)
print(grid_size)
plt.plot(res)
plt.show()