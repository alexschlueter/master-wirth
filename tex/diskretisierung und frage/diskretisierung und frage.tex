\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{mathtools}
\usepackage{csquotes}

\newcommand{\sphere}{\mathbb{S}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\msp}{\mathcal{M}}
\DeclarePairedDelimiterX{\norm}[1]{\lVert}{\rVert}{#1}

\title{Dimensionsreduktion zur Dynamic Spike Super-resolution}
\author{Alexander Schlüter}

\begin{document}
\maketitle
\section{Diskretisierung in 2D}
Wir wählen diskrete Winkel $\Theta^m=\{\theta_1,\dots, \theta_m\}\subset\sphere^1$ sowie Richtungen $D^p=\{n_1,\dotsc n_p\}\subset\sphere^1$.
Die Maße $u_{-K},\cdots,u_{K}, \mu(\theta_1,\cdot),\dotsc,\mu(\theta_m,\cdot),\nu(n_1,\cdot),\dotsc,\nu(n_p,\cdot)$ werden jeweils auf einem gleichmäßigen, quadratischen Gitter auf $[0,1]^2$ mit $L^2$ Gitterpunkten $g_1,\dotsc,g_{L^2}$
diskretisiert und als Vektoren mit $L^2$ Einträgen gespeichert.

Der Messoperator $B:\msp([0,1]^2)\to\RR^n$ wird diskretisiert als Matrix

\begin{equation*}
    M_B = (B\delta_{g_1},\dotsc,B\delta_{g_{L^2}}) \in\RR^{n\times L^2}.
\end{equation*}

Für jedes $\theta\in\Theta^m$ wird der Linienprojektionsoperator 
\begin{equation*}
    P^\theta:\msp([0,1]^2)\to\msp(\RR),\quad \xi\mapsto [x\mapsto \theta\cdot x]_\#\xi
\end{equation*}
folgendermaßen diskretisiert: Wähle Radien $r_1<r_2<\dotsc<r_q\in\RR$ und teste für jeden Gitterpunkt $g$, zwischen welchen Radien die Projektion $P^\theta\delta_{g}$ liegt. Setze den entsprechenden Eintrag in der Matrix $M^\theta\in\RR^{(q-1)\times L^2}$ auf 1:
\begin{equation*}
    (M^\theta)_{ij} = \begin{cases}
        1, &\text{falls }\theta\cdot g_j\in [r_i, r_{i+1})\\
        0, &\text{sonst}
    \end{cases},\quad 1\leq i \leq q-1,\quad 1\leq j\leq L^2
\end{equation*}
Verfahre analog mit den Operatoren 
\begin{align*}
    P^n:\msp([0,1]^2)\to\msp(\RR),&\quad \xi\mapsto [x\mapsto n\cdot x]_\#\xi \\
    P^k:\msp([0,1]^2)\to\msp(\RR),&\quad \xi\mapsto [x\mapsto (1, k\tau)\cdot x]_\#\xi
\end{align*}
und erhalte Matrizen $M^n, M^k\in\RR^{(q-1)\times L^2}$ für $n\in D^p$ und $k=-K,\dotsc,K$.

Wir beschränken uns auf positive Maße und setzen die angestrebte Gesamtmasse $\gamma>0$ als bekannt voraus. Das diskrete Optimierungsproblem lautet
\begin{gather*}
    \begin{align*}
\min_{\substack{u_{-K},\dotsc,u_{K}\in\RR_+^{L^2}\\
\mu(\theta,\cdot)\in\RR_+^{L^2}, \theta\in\Theta^m\\
\nu(n,\cdot)\in\RR_+^{L^2}, n\in D^p}} \sum_k \norm{M_B u_k - y_k}_2^2 &+ \sum_{k,\theta}\norm{M^\theta u_k-M^k\mu(\theta,\cdot)}_2^2 \\[-2em]
&+\sum_{\theta, n}\norm{M^n\mu(\theta,\cdot) - M^\theta\nu(n,\cdot)}_2^2
    \end{align*}\\
    \text{subject to}\\
\begin{alignedat}{2}
    &\norm{u_k}_1 \leq \gamma, &&k=-K\dotsc,K \\
    &\norm{\mu(\theta, \cdot)}_1 \leq \gamma, &\quad&\theta\in\Theta^m \\
    &\norm{\nu(n,\cdot)}_1 \leq \gamma, &&n\in D^p
\end{alignedat}
\end{gather*}
Die einzelnen Matrizen werden zu einer Systemmatrix zusammengefügt und das entstehende quadratische Programm mit der Pythonbibliothek \textit{cvxopt} gelöst.

\section{Frage zum Geschwindigkeitsproblem}
In Ihrer Mail vom 21.08.18 haben Sie geschrieben
\begin{displayquote}
    gleichzeitig können wir ein vektorwertiges Maß $V\in\mathcal \msp(R^d;R^d)$ berechnen durch Lösen von
    \begin{equation}\label{eq:V}
[x\mapsto\theta\cdot x]_\#V=[(y,w)\mapsto y]_\#(w\mapsto w\mu(\theta,w)) \quad\text{für alle $\theta\in \sphere^{d-1}$}
    \end{equation}

(dies lässt sich eindeutig lösen); dieses V entspricht $[(x,v)\mapsto x]_\#((x,v)\mapsto v\lambda(x,v))$ und ist somit die Geschwindigkeit der Partikel. 
\end{displayquote}
Wenn ich für Positionen $x_1,\dotsc,x_N\in\RR^d$, Geschwindigkeiten
$v_1,\dotsc,v_N\in\RR^d$ und Gewichte $\alpha_1,\dotsc,\alpha_N\in\RR$ das Maß
\begin{equation*}
    \lambda = \sum_{i=1}^N \alpha_i\delta_{(x_i,v_i)}
\end{equation*}
betrachte, erhalte ich für $\theta\in\sphere^{d-1}$
\begin{align*}
    \mu(\theta, \cdot) &= [(x,v)\mapsto(\theta\cdot x, \theta\cdot v)]_\#\lambda \\
    &=\sum_{i=1}^N \alpha_i\delta_{(\theta\cdot x_i,\theta\cdot v_i)}.
\end{align*}
Wenn ich die Notation richtig verstehe, bekomme ich
\begin{align*}
    [(y,w)\mapsto y]_\#(w\mapsto w\mu(\theta,w))
    &=[(y,w)\mapsto y]_\#\sum_{i=1}^N \alpha_i\begin{pmatrix}
      \theta\cdot x_i\\  
      \theta\cdot v_i\\  
    \end{pmatrix}\delta_{(\theta\cdot x_i,\theta\cdot v_i)} \\
    &=\sum_{i=1}^N \alpha_i\begin{pmatrix}
      \theta\cdot x_i\\  
      \theta\cdot v_i\\  
    \end{pmatrix}\delta_{\theta\cdot x_i}
\end{align*}
Möglicherweise meinten Sie auch $((y,w)\mapsto w\mu(\theta,y,w)$, dann bekäme ich $\sum \alpha_i (\theta\cdot v_i)\delta_{\theta\cdot x_i}$.
Allerdings ist doch 
\begin{equation*}
    [(x,v)\mapsto x]_\#((x,v)\mapsto v\lambda(x,v))=\sum_{i=1}^N \alpha_i v_i \delta_{\theta\cdot x_i}.
\end{equation*}
Ich sehe nicht, wie ich durch Lösen der Gleichung~\ref{eq:V} von $\theta\cdot v_i$ zu $v_i$ innerhalb der Summe komme.
\end{document}