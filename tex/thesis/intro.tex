% !TeX root = ./thesis.tex
% \chapter{Einleitung}
\addchap{Einleitung}
Wir beschäftigen uns mit dem Problem der Super-Resolution
(auf Deutsch auch Super-Auflösung), % ?????????????????
also mit der Fragestellung, wie feinskalige Information
aus der Messung eines grobskaligen Signals rekonstruiert werden können.
Konkret stellen wir uns eine Sammlung von Punktquellen vor,
bei denen es sich zum Beispiel um 
% Blutkörperchen,
fluoreszierende Moleküle oder
Sterne handeln kann.
% Die zugrundeliegende Information kann zum Beispiel aus den Positionen
% von Blutkörperchen, ... oder sogar von einzelnen Molekülen sein, oder 
% Wie die obigen Beispiele zeigen, ist diese Information häufig 
% kontinuierlich parametrisiert.
Mathematisch wird eine solche Punkteverteilung repräsentiert durch
eine Linearkombination von Dirac-Maßen
\begin{equation*}
    \smt=\sum_{i=1}^N\weight_i\dirac_{\sx_i}
\end{equation*}
mit Intensitäten $\weight_i\in\CC$ an Positionen $\sx_i$ innerhalb
eines Gebietes $\paramsp\subset\RR^d$.

Als Messung kommen Bildgebungsverfahren wie zum Beispiel die Mikroskopie,
PET-Scanner in der Medizin oder die Teleskopie in der Astronomie in Frage.
Wir stellen uns vor, dass die Messung insofern unvollständig ist,
als feinskalige Informationen im Laufe des Messprozesses verloren gehen.
Im Fall der optischen Bildgebungsverfahren führt zum Beispiel die Beugung des Lichtes
an der Blende dazu, 
dass Punktquellen nicht als scharfe Punkte, sondern als verschwommene
Scheiben abgebildet werden.
Dies stellt eine physikalische Auflösungsgrenze dar, welche auch
als \emph{Abbe-Limit} bekannt ist.
% Dies kann in der Anwendung verschiedene Ursachen haben:
% Elektrische Bildgebungsverfahren sind häufig limitiert durch die
% Größe eines Pixels sowie dem statistischen Photonenrauschen.

Im Artikel \citetitle{Candes2014} \cite{Candes2014} wurde von \citeauthor{Candes2014}
gezeigt, dass sich die Positionen der Punktquellen durch ein konvexes
Minimierungsproblem der Form
\begin{equation*}\tag{\erstat}\label{eq:erstat}
    \min_{\smp\in\msp(\domd)}\TV{\smp}
    \quad\text{s.t.}\quad \mopstat\smp=\sdat
\end{equation*}
über dem Raum der Maße $\msp(\paramsp)$ exakt rekonstruieren lassen.
Hierbei modelliert ein Operator $\mopstat$ den Messprozess,
$\sdat=\mopstat\smt$ sind die gemessenen Daten und $\TV{\cdot}$ ist die sogenannte
 Totale-Variations-Norm
eines Maßes. Dies ist möglich, solange die aufzulösenden Punkte
einen Mindestabstand nicht unterschreiten, welcher von dem Frequenzbereich
abhängt, der durch den 
%Operator $\mopstat$
Messprozess abgeschnitten wird.
Der Beweis nutzt auf entscheidende Weise die Dualität zwischen dem Raum
der Maße und dem Raum der stetigen Funktionen, welche als der \emph{Darstellungssatz
von Riesz} bekannt ist.
Zu einer gegebenen Punkteverteilung werden sogenannte \emph{duale Zertifikate}
konstruiert. Hierbei handelt es sich um trigonometrische Polynome,
welche eine von den Positionen der Punktquellen abhängige Interpolationsaufgabe
lösen müssen. Die Existenz eines dualen Zertifikates garantiert, dass
die tatsächliche Punkteverteilung die eindeutige Lösung des Problems
\eqref{eq:erstat} ist.

In der Realität stehen die Dinge selten still: Sterne wandern am Himmel,
Zellen bewegen sich in der Blutbahn, der Patient zuckt im Computertomographen.
Der Experimentator befindet sich in einem Dilemma: Misst er über einen
langen Zeitraum, so können viele Daten für eine Rekonstruktion gesammelt
werden. Währenddessen wird sich die Probe jedoch über eine weite
Strecke bewegt haben und das Bild verschwimmen.
% Allerdings wird sich die Probe währenddessen über eine weite
% Strecke bewegt haben und das Bild verschwimmen.

Verkürzt er die Messdauer, so ist die Bewegung im Vergleich zur Größe
der abzubildenden Struktur möglicherweise vernachlässigbar. Allerdings
können im kurzen Zeitraum nur wenige Daten gesammelt werden, was
eine Rekonstruktion erschwert.

Die Lösung des Dilemmas ist eine Berücksichtigung der Bewegung in der Modellierung,
sodass diese während der Rekonstruktion wieder herausgerechnet werden kann.
Die entsprechende Verallgemeinerung von \eqref{eq:erstat} für lineare Dynamiken wurde von
\citeauthor{Alberti2018} unter dem Begriff "Dynamic Spike Super-resolution" \cite{Alberti2018} 
veröffentlicht und führt auf ein sehr ähnliches TV-Minimierungsproblem.
Allerdings treten statt der Orte $x_i\in\RR^d$ nun Orte und Geschwindigkeiten
$(x_i,v_i)\in\RR^{d}\times\RR^d$ als Variablen auf. Das zu rekonstruierende
Maß hat also die Form
\begin{equation*}
    \dmt=\sum_{i=1}^N\weight_i\dirac_{(x_i,v_i)}\,.
\end{equation*}
Dieses \emph{Phasenraum-Lifting} des Problems erlaubt es, vieles von der Theorie
zu \eqref{eq:erstat} auf das dynamische Problem zu übertragen.


Doch das Lifting ist nicht ohne Nachteil: Das resultierende Problem
hat Dimension $2d$ statt $d$. Ein üblicher Diskretisierungsansatz
besteht darin, ein Gitter über den Parameterraum zu legen. Für $d=3$
wäre hierzu ein sechsdimensionales Gitter notwendig, was schnell
zu numerischen Problemen aufgrund der Vielzahl an Variablen führt.

In dieser Arbeit wird eine Formulierung untersucht, welche in gewisser Weise
"zwischen" dem statischen und dem dynamischen Problem angesiedelt ist:
Für jeden Richtungsvektor $\thins$ wird die gemeinsame Projektion
der Orts- und Geschwindigkeitsvariablen auf $\theta$ betrachtet, was
auf ein Maß der Form
\begin{equation*}
    \rmt_\theta=\sum_{i=1}^N\weight_i\dirac_{(\thdot{x_i},\thdot{v_i})}
\end{equation*}
führt. Es wird ein dimensionsreduziertes Problem formuliert, welches
die Dynamik der Teilchen berücksichtigt, aber nur noch Variablen
der Dimension $d+1$ statt $2d$ enthält.

Die Arbeit ist wie folgt organisiert:
Zu Beginn werden die mathematischen Werkzeuge
eingeführt, welche später benötigt werden. Dies ist die Theorie komplexwertiger Maße, 
darunter der Zerlegungssatz von Lebesgue und der Darstellungssatz von Riesz.

Anschließend wird die Methode der Super-Resolution mittels konvexer Optimierung
in \cref{chap:srconv} vorgestellt. Hier wird ein duales Problem hergeleitet und der Begriff der dualen
Zertifikate definiert, was es erlaubt, Aussagen über Existenz und Eindeutigkeit von Lösungen zu treffen. 

Der Super-Resolution von bewegten Teilchen wenden wir uns in \cref{chap:dynamic} zu.
Nach einer Vorstellung des Modells werden Situationen untersucht, in welchen 
dynamische duale Zertifikate existieren. Hierfür wird der Begriff des Durchschnittszertifikates
sowie der Begriff der Geisterteilchen zentral sein.

Es folgt die Vorstellung einer neuen Methode zur Dimensionsreduktion des dynamischen
Super-Resolution Problems in \cref{chap:dimreduc}. Verschiedene mögliche Formulierungen werden
diskutiert. Es wird bewiesen, dass die Methode trotz reduzierter Dimension des Problems
das Zielmaß unter bestimmten Voraussetzungen noch exakt rekonstruieren kann.

Die Numerik ist Thema des letzten Kapitels.
Hier werden zunächst zwei Verfahren aus der Literatur zur numerischen Lösung 
des Minimierungsproblems beschrieben: Eine einfache Gitter-Diskretisierung sowie der sogenannte
ADCG-Algorithmus.
Der Schwerpunkt liegt jedoch in der Entwicklung einer geeigneten Diskretisierung für die
dimensionsreduzierte Methode, welche 
schließlich anhand eines hierfür generierten Datensatzes mit mehreren anderen Methoden verglichen wird.
