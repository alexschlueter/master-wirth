\documentclass[german]{scrartcl}
\usepackage{biblatex}
\bibliography{mrec.bib}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{mathtools}
\usepackage{csquotes}

\usepackage{hyperref}
\usepackage{cleveref}

\newcommand{\sphere}{\mathbb{S}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\screps}{\mathcal{E}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\msp}{\mathcal{M}}
\newcommand{\allms}{\mathcal{Z}}
\newcommand{\radon}{\mathcal{R}}
\newcommand{\borel}{\mathcal{B}}
\newcommand{\fourier}{\mathcal{F}}
\newcommand{\intd}{\,\mathrm{d}}
\DeclarePairedDelimiterX{\norm}[1]{\lVert}{\rVert}{#1}
\DeclarePairedDelimiterX{\sqbrak}[1]{[}{]}{#1}
\DeclarePairedDelimiterX\set[1]\lbrace\rbrace{\def\given{\;\delimsize\vert\;}#1}
\newcommand{\TV}[1]{\norm{#1}_\text{TV}}
\newcount\colveccount
\newcommand*\colvec[1]{
        \global\colveccount#1
        \begin{pmatrix}
        \colvecnext
}
\def\colvecnext#1{
        #1
        \global\advance\colveccount-1
        \ifnum\colveccount>0
                \\
                \expandafter\colvecnext
        \else
                \end{pmatrix}
        \fi
}

\newtheorem{prop}{Proposition}

\title{Dimensionsreduktion von dynamischer Super-resolution}
\author{Alexander Schlüter}

\begin{document}
\maketitle
\section{Umformulierung der Nebenbedingungen}
Definiere
\begin{equation*}
    \allms\coloneqq \msp(\RR^d)^{2K+1}\times\msp(\sphere^{d-1}\times\RR^2)\times\msp(\sphere^1\times\RR^d).
\end{equation*}
Das ursprüngliche Optimierungsproblem lautete
\begin{gather*}
    \min_{(u,\mu,\nu)\in\allms}\TV{\mu}\\
    \text{subject to}\\
\begin{alignedat}{2}
    &Bu_k=y_k, &&k=-K\dotsc,K, \\
    &[x\mapsto\theta\cdot x]_\#u_k =[(y,w)\mapsto y+k\tau w]_\#\mu(\theta,\cdot), &&k=-K,\dotsc,K,\theta\in\sphere^{d-1},\\
    & [(y,w)\mapsto n\cdot(y,w)]_\#\mu(\theta,\cdot)=[x\mapsto\theta\cdot x]_\#\nu(n,\cdot)&\quad&\theta\in\sphere^{d-1},n\in\sphere^1.
\end{alignedat}
\end{gather*}
Nach meinem Verständnis sind aber für allgemeine Maße $\mu, \nu$ die Ausdrücke "$\mu(\theta,\cdot)$", "$\nu(n,\cdot)$"
nicht wohldefiniert (oder hat das etwas mit Disintegration von Maßen zu tun?).

Es sei $\omega_{d-1}$ das normierte Oberflächenmaß auf $\sphere^{d-1}$.
Angelehnt an \cite{Boman2009} würde ich die Radon-Transformation für Maße definieren als
\begin{gather*}
    \radon_d:\msp(\RR^d)\to\msp(\sphere^{d-1}\times\RR)\\
    u\mapsto\radon_d u\\
    \text{definiert für beliebige Borelmengen $E\in\borel(\sphere^{d-1}\times\RR)$ durch}\\
    \radon_d u(E)\coloneqq\int_{\sphere^{d-1}}([x\mapsto\theta\cdot x]_\#u)(E_\theta)\intd\omega_{d-1}(\theta)
\end{gather*}
wobei $E_\theta=\set{x\in\RR\given (\theta,x)\in E}$ der $\theta$-Schnitt von $E$ sei.
Dies ist äquivalent zu
\begin{equation*}
    \radon_d u(E)=\int_{\sphere^{d-1}}([x\mapsto(\theta, \theta\cdot x)]_\#u)(E)\intd\omega_{d-1}(\theta).
\end{equation*}
Versteht man $[x\mapsto\theta\cdot x]_\#u$ als einen Übergangskern $\sphere^{d-1}\to\RR$, so kann man die Definition kürzer ausdrücken als
$\radon_d u=\omega_{d-1}\otimes[x\mapsto\theta\cdot x]_\#u$.

Um die fragwürdige Auswertung von $\mu$ an einzelnen $\theta\in\sphere^{d-1}$ in der zweiten Nebenbedingung zu umgehen, würde ich diese ersetzen durch
\begin{equation*}
\radon_d u_k=\sqbrak*{\colvec{3}{\theta}{y}{w}\mapsto
\colvec{2}{\theta}{y+k\tau w}}_\#\mu,\quad k=-K,\dotsc,K
\end{equation*}

Für die dritte Nebenbedingung möchte ich ähnlich verfahren und definiere
\begin{gather*}
    \radon_\mu:\msp(\sphere^{d-1}\times\RR^2)\to\msp(\sphere^{1}\times\sphere^{d-1}\times\RR)\\
    \radon_\nu:\msp(\sphere^{1}\times\RR^d)\to\msp(\sphere^{1}\times\sphere^{d-1}\times\RR)\\
    \radon_\mu \mu(E)\coloneqq\int_{\sphere^{1}}
    % (\sqbrak*{\colvec{3}{\theta}{y}{w}\mapsto\colvec{2}{\theta}{n_1y+n_2w}}_\#\mu)(E_n)d\omega_{1}(n), E\in\borel(\sphere^{1}\times\sphere^{d-1}\times\RR)\\
    (\sqbrak{(\theta,y,w)\mapsto(n,\theta,n\cdot(y,w))}_\#\mu)(E)\,\mathrm{d}\omega_{1}(n)\\
    \radon_\nu \nu(E)\coloneqq\int_{\sphere^{d-1}}
    (\sqbrak{(n,x)\mapsto(n,\theta,\theta\cdot x)}_\#\nu)(E)\intd\omega_{d-1}(\theta)\\
    \text{für alle $E\in\borel(\sphere^{1}\times\sphere^{d-1}\times\RR)$}.
    % (\sqbrak*{\colvec{3}{\theta}{y}{w}\mapsto\colvec{2}{\theta}{n_1y+n_2w}}_\#\mu)(E_n)d\omega_{1}(n), E\in\borel(\sphere^{1}\times\sphere^{d-1}\times\RR)\\
\end{gather*}
Das neue Minimierungsproblem lautet dann
\begin{equation}\label{eq:minneu}
\begin{gathered}
    \min_{(u,\mu,\nu)\in\allms}\TV{\mu}\\
    \text{subject to}\\
\begin{alignedat}{2}
    &Bu_k=y_k, &&k=-K\dotsc,K, \\
    &\radon_d u_k=\sqbrak*{(\theta,y,w)\mapsto
(\theta,y+k\tau w)}_\#\mu, &&k=-K,\dotsc,K,\\
    & \radon_\mu\mu = \radon_\nu\nu.&\quad&
\end{alignedat}
\end{gathered}
\end{equation}

\section{Recovery of complex measures}
Nach meinem Verständnis ist der Schlüssel zum Beweis der noiseless recovery Proposition A.1 aus \cite{Candes2013}.
Diese stellt den Zusammenhang zwischen den Lösungen des TV-Minimierungsproblems und den niedrigfrequentigen Polynomen ("dualen Zertifikaten") her.

Für $N\in\NN, t_1,\dotsc,t_N\in[0,1],\alpha_1,\dotsc,\alpha_N\in\CC$ sei
\begin{equation*}
    x=\sum_{j=1}^N\alpha_j\delta_{t_j}
\end{equation*}
ein diskretes komplexwertiges Maß. Sei $f_c\in\NN$ ein Frequenzlimit und $n=2f_c+1$ die Anzahl
der gemessenen Frequenzen. Weiter sei $\msp([0,1],\CC)$ der Raum der komplexwertigen Maße auf $[0,1]$.
Der Messoperator wird definiert als
\begin{equation*}
    \fourier_n:\msp([0,1],\CC)\to\CC^n,\quad z\mapsto\left(\int_0^1e^{-i2\pi kt}\intd z(t)\right)_{k\in\ZZ,|k|\leq f_c}
\end{equation*}
und von $x$ sollen nur die gemessenen Daten $y=\fourier_n x$ als bekannt vorausgesetzt werden.
Es wird das folgende Minimierungsproblem betrachtet:
\begin{equation}\label{eq:min1}
    \min_{\tilde{x}}\TV{\tilde{x}}\qquad\text{subject to}\qquad\fourier_n\tilde{x}=y
\end{equation}
\begin{prop}[\cite{Candes2013}, Prop. A.1]
Für jeden Vektor $v\in\CC^N$ mit $|v_j|=1, j=1,\dotsc,N$ existiere ein trigonometrisches 
Polynom
\begin{equation*}
    q(t)=\sum_{k=-f_c}^{f_c}c_k e^{i2\pi kt}
\end{equation*}
mit den Eigenschaften
\begin{enumerate}
    \item $q(t_j)=v_j,\quad j=1,\dotsc,N$
    \item $|q(t)|<1,\quad t\in[0,1]\setminus\set{t_1,\dotsc,t_N}$.
\end{enumerate}
Dann ist $x$ die eindeutige Lösung zu Problem \eqref{eq:min1}.
\end{prop}
Im Beweis wird für eine beliebige Lösung $\hat{x}$ von \eqref{eq:min1} die Differenz $h=\hat{x}-x$ betrachtet.
Da $x$ und $\hat{x}$ die Nebenbedingung erfüllen, wissen wir
\begin{equation*}
    \fourier_n h=\fourier_n\hat{x} - \fourier_n{x}=0
\end{equation*}
und da jedes Polynom $q$ aus einer Linearkombination von niedrigfrequentigen trigonometrischen Polynomen besteht, folgt
\begin{equation*}
    \int_0^1 q(t)\intd h(t)=0.
\end{equation*}
Nach dem Zerlegungssatz von Lebesgue existieren Maße $h_T, h_{T^c}\in\msp([0,1],\CC)$ mit $h_T\ll |x|,h_{T^c}\perp |x|$ und 
\begin{equation*}
    h=h_T+h_{T^c}.
\end{equation*}
Es wird nun eine Ungleichung zwischen den TV-Normen von $h_T$ und $h_{T^c}$ hergeleitet über 
\begin{equation*}
    0=\int_0^1 q(t)\intd h(t)=\int_0^1 q(t)\intd h_T(t)+\int_0^1 q(t)\intd h_{T^c}(t)
    =\TV{h_T}+\int_0^1 q(t)\intd h_{T^c}(t),
\end{equation*}
wobei für die letzte Gleichheit $q$ geeignet gewählt werden muss.

Ich habe versucht, den Beweis für unsere Zwecke zu adaptieren. Zunächst kann man eine weitere 
Lösung $(\hat{u},\hat{\mu},\hat{\nu})\in\allms$ des Minimierungsproblems~\eqref{eq:minneu} betrachten.
Die Differenz $(u^h,\mu^h,\nu^h)=(\hat{u},\hat{\mu},\hat{\nu})-(u,\mu,\nu)$ erfüllt nun die Bedingungen
\begin{align*}
    &Bu^h_k=0,\\
    &\radon_d u^h_k=\sqbrak*{(\theta,y,w)\mapsto(\theta,y+k\tau w)}_\#\mu^h, \qquad k=-K,\dotsc,K
\end{align*}
wobei ich für $B$ auch erstmal von einer abgeschnittenen Fouriermessung ausgehe:
\begin{equation*}
    Bu^h_k = \left(\int_{\RR^d}e^{-i2\pi l\cdot x}\intd u^h_k(x)\right)_{l\in\ZZ^d, \norm{l}_\infty\leq f_c}.
\end{equation*}
Da ich die TV-Norm von $\mu^h$ abschätzen möchte, muss ich die zweite Nebenbedingung nutzen.
Mein Ansatz ist, für $l\in\ZZ^d$ eine Funktion $\tilde{q}_l:\sphere^{d-1}\times\RR\to\CC$ zu suchen mit
\begin{equation}\label{eq:ql}
    \int_{\sphere^{d-1}}\tilde{q}_l(\theta, \theta\cdot x)\intd\omega_{d-1}(\theta)\overset{!}{=}e^{-i2\pi l\cdot x}\qquad\forall x\in\RR^d,
\end{equation}
denn dann würde für $k=-K,\dotsc,K$ gelten 
\begin{align*}
    \int_{\sphere^{d-1}\times\RR^2}\tilde{q}_l(\theta, y+k\tau w)\intd\mu^h(\theta,y,w)
    &=\int_{\sphere^{d-1}\times\RR}\tilde{q}_l(\theta,s)\intd\radon_d u^h_k(\theta,s)\\
    &=\int_{\RR^d}\int_{\sphere^{d-1}}\tilde{q}_l(\theta,\theta\cdot x)\intd\omega_{d-1}(\theta)\intd u^h_k(x)\\
    &=\int_{\RR^d}e^{-i2\pi l\cdot x}\intd u^h_k(x)\\
    &=0\,.
\end{align*}
Meine Hoffnung ist, mit Linearkombinationen der $\tilde{q}_l$ den Beweis ähnlich weiterführen zu können wie Candès und Fernandez-Granda.
Dazu hätte ich gerne einen konkreten Ausdruck für die $\tilde{q}_l$.
\section{Frage}
Bei den $\tilde{q}_l$ aus \cref{eq:ql} handelt es sich ja um die inverse duale Radon-Transformation von $e^{-i2\pi l\cdot x}$.
Ich würde gerne einen konkreten Ausdruck für die $\tilde{q}_l$ finden, stecke hier aber fest.
Laut \cite{Hertle1984}, Thm 2.1 ist die duale Radon-Transformation $\screps(\sphere^{d-1}\times\RR)\to\screps(\RR^d)$ surjektiv,
also sollte nach meinem Verständnis eine Funktion $\tilde{q}_l\in C^\infty(\sphere^{d-1}\times\RR)$ existieren,
die das Gewünschte erfüllt. Haben Sie dazu eine Idee?

\printbibliography
\end{document}