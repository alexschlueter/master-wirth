import itertools
import time

import cvxopt
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np

import l1regls
from models.dynamic import DynamicSuperresModel
from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel
from discretization import *
from solvers import *



# np.set_printoptions(threshold=np.inf)


# @profile
def main():
    disc_single = Discretization(
        tau = 1,
        K = 0,
        num_dirs = 0,
        num_rads = 0,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 0,
        adaptive_grids=True
    )
    disc3 = Discretization(
        tau = 1,
        K = 1,
        # num_dirs = 3,
        # num_rads = 3,
        # grid_size_nu = 3,
        # grid_size_mu = 3,
        # num_extra_ns = 3,

        # num_extra_ns = 11,
        # num_rads = 11,

        # grid_size_nu = 60,
        # grid_size_mu = 60,

        # num_dirs = 17,
        # num_rads = 17,
        # # num_extra_ns = 17,
        # num_extra_ns = 0,
        # grid_size_nu = 20,
        # grid_size_mu = 20,

        # num_dirs = 100,
        # num_rads = 100,
        # num_extra_ns = 100,
        # grid_size_nu = 640,
        # grid_size_mu = 640,

        # grid_size_nu = 17,
        # grid_size_mu = 17,

        num_dirs = 30,
        # num_dirs = 0,
        # num_rads = 30,
        num_rads = 100,
        # num_extra_ns = 30,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 100,
        adaptive_grids=True
    )
    # disc = disc_single
    disc = disc3
    # support = np.array([[0.5,0.5,0.0,0.0]]).transpose()
    # weights = np.array([1.0])
    # support = np.array([[0.5,0.5,0.3,0.3]]).transpose()
    # weights = np.array([1.0])
    # support = np.array([[0.3,0.4,0.1,-0.1], [0.5,0.5,-0.1,0.1]]).transpose()
    # weights = np.array([1.0, 1.0])

    # support = np.array([[0.3,0.4,0.1,0.1], [0.5,0.5,0.2,0.1]]).transpose()
    support = np.array([[0.5,0.47,0.2,0.0], [0.5,0.53,-0.2,0.0]]).transpose()
    weights = np.array([1.0, 1.0])

    # support = np.array([[0.3,0.4,0,0]]).transpose()
    # weights = np.array([1.0])
    # support = np.array([[0.3,0.4,0,0],[0.5,0.5,0,0]]).transpose()
    # weights = np.array([1.0,1.0])
    # model_static = FourierNDModel([np.arange(-2, 3), np.arange(-2, 3)], [1, 1])
    fc = 1
    model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])
    # model_static = IdentityModel([1, 1], 2*[p.grid_size_nu])
    model = DynamicSuperresModel(model_static, 1, disc.tau, disc.K)

    target = model.apply(support, weights)

    print("Stage 1: Main objective")
    Msys, y = disc.build_main_objective(model_static, target)

    l1_target = np.linalg.norm(weights, ord=1)
    solver = SeparableConeSolver(disc, Msys, y, l1_target, nonneg=False)
    # solver = CVXQuadraticProgramSolver(Msys, y, l1_target, disc, sparse=True, kktsolver=True)

    print("Start opt...")
    start = time.time()
    mu, nu = solver.solve()
    end = time.time()
    print(end-start)
    print("Finished opt...")

    fname = "data/res_"+time.strftime("%Y%m%d-%H%M%S")
    print("Saving result to {}.npy".format(fname)) 
    np.save(fname, (mu, nu))

    los = list_of_sources(nu[-disc.K-1])
    print(los)
    tp = match_sources(support.transpose()[:,:2], los)
    prec = tp/len(los)
    rec = tp/len(support.transpose())
    f1 = 2*prec*rec/(prec+rec)
    print(prec, rec, f1)

    # print(res[-(p.K+1)*p.gs_nu_sq:-p.K*p.gs_nu_sq].reshape((p.grid_size_nu, -1)))

    # print("Stage 2: Reconstruct velocities")

    # Vmat, b = disc.build_vel_objective(mu)

    # # Vres = np.linalg.lstsq(Vmat, b)[0]
    # # Vres = sp.linalg.lsmr(Vmat, b, damp=1)[0]
    # Vres = np.array(l1regls.l1regls(cvxopt.matrix(Vmat), cvxopt.matrix(b))).reshape(2, disc.grid_size_nu, -1)
    # fname = "data/resV_"+time.strftime("%Y%m%d-%H%M%S")
    # print("Saving velocity result to {}.npy".format(fname)) 
    # np.save(fname, Vres)

    # fig = plt.figure(figsize=(50,50))
    # for k in range(disc.num_time_steps):
    #     fig.add_subplot(1,disc.num_time_steps+2,k+1).imshow(nu[-disc.num_time_steps+k].transpose(),
    #                                                    interpolation="none", origin="lower")
    # for i in range(2):
    #     fig.add_subplot(1,disc.num_time_steps+2,disc.num_time_steps+1+i).imshow(Vres[i].transpose(),
    #                                                    interpolation="none", origin="lower")
    # fig.savefig("data/fig_"+time.strftime("%Y%m%d-%H%M%S"))

main()
