import numpy as np

from loss import LeastSquaresLoss
from models.fourier import FourierNDModel
from models.radon import Radon2DModel
from models.dynamic import DynamicSuperresModel
from adcg import run_adcg


def main():
    loss = LeastSquaresLoss()
    # model_static = Fourier1DModel(20, 1)
    # support = np.array([[0.2, 0.3, 0.8]])
    # weights = np.array([1, 2, 4])
    # model_static = FourierNDModel([np.arange(-1, 2), np.arange(-1, 2)], [1, 2])
    # model_static = FourierNDModel([np.arange(-10, 11), np.arange(-12, 13)], [1, 2])
    # support = np.array([[0.2, 0.4], [0.2, 1], [0.3, 1.2], [0.8, 1]]).transpose()
    # weights = np.array([1.0,2.0,3.0,4.0])

    # model_static = FourierNDModel([np.arange(-10, 11)], [2])
    # support = np.array([[0.2, 0.3, 0.8]])
    # weights = np.array([1, 2, 4])

    # model_static = Radon2DModel(21, 21, 21, 0.1)
    # support = np.array([[0.2, 0.3], [0.2, 0.5], [0.5, 0.6]]).transpose()
    # weights = [1,2,3]

    fc = 3
    model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])

    # support = np.array([[0.1, 0.1], [0.5, 0.5]]).transpose()
    # weights = [1, 2]
    # support = np.array([[0.5, 0.5]]).transpose()
    # weights = [1]

    model = DynamicSuperresModel(model_static, 0.7, 1, 2, 20)
    support = np.array([[0.5, 1, 0.2, 0.2], [0.3, 1, 0.1, -0.4], [0.6, 1.4, -0.2, 0.6]]).transpose()
    # support = np.array([[0.5, 0.99, 0.2, 0.2], [0.3, 0.99, 0.1, -0.4], [0.6, 0.3, -0.2, 0.1]]).transpose()
    # support = np.array([[0.2, 0.5]]).transpose()
    weights = [1, 2, 3]
    # model = model_static

    target = model.apply(support, weights)

    def cb_fun(old_support, cb_support, cb_weights, output, old_obj_val):
        new_obj_val, _ = loss.loss_and_grad(output-target)
        return old_obj_val - new_obj_val < 1e-4

    print(run_adcg(model, loss, target, np.sum(weights), cb_fun))

if __name__ == '__main__':
    main()
