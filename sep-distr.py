import os
import shutil
import sys

import numpy as np

from utils import *

jobid = sys.argv[1]
print("I am job #{}".format(jobid))

# folder = "sepdist_4-20parts/"+jobid
folder = jobid
try:
    os.mkdir(folder)
except FileExistsError:
    pass

shutil.copy(os.path.realpath(__file__), folder+"/")

num_til_save = 10000
min_num_srcs = 4
max_num_srcs = 20
minsep = 0.03
maxsep = 0.06
wmin = 0.9
wmax = 1.1


num_sep_bins = 100
sep_bins = np.linspace(0, 0.08, num_sep_bins)
sep_counts = np.zeros(num_sep_bins)

outer_it = 0
while True:
    print("Outer-It {} Total {}".format(outer_it, outer_it*num_til_save))
    seps = []
    for it in range(num_til_save):
        num_sources = np.random.randint(min_num_srcs, max_num_srcs+1)
        support = np.empty((num_sources, 4))
        for i in range(num_sources):
            while True:
                pos = np.random.rand(2)
                vel = np.random.rand(2)-0.5
                before = pos-vel
                after = pos+vel
                if np.all((0 < before) & (0 < after) & (before < 1) & (after < 1)):
                    support[i] = np.hstack((pos, vel))
                    break

        seps.append(min_dyn_sep(support, 1, 1))

    announce_save("separations", "seps_{}".format(outer_it+1), seps, folder=folder)
    outer_it += 1
        # sep = min_dyn_sep(support, 1, 1)
#         print(sep)
#         idx = np.searchsorted(sep_bins, sep, side="right")-1
#         if sep_counts[idx] <= num_gts/num_sep_bins:
#             sep_counts[idx] += 1
#             break
#         # if  minsep < sep < maxsep:
#         #     break

#     weights = wmin + np.random.rand(num_sources)*(wmax-wmin)
#     # announce_save("generated sources", "gt_{}".format(it+1), (None, support, weights), folder=folder, with_time=False)
#     print("ACCEPT!!!!!!!!!!!!", support)
# print(sep_counts)
# print(np.min(sep_counts))

# announce_save("separations", "separations", seps)
# plt.hist(seps, 1000)
# plt.savefi