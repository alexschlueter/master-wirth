import numpy as np
import cvxopt

# from calljulia import jnnlasso
from optimization.nnlasso import nnlasso

class LeastSquaresLoss:
    @staticmethod
    def loss_and_grad(point):
        norm = np.linalg.norm(point)
        return norm, point/norm 

    # @classmethod
    # def optimize_weights(cls, forward_model, support, target, max_mass):
    #     def obj_fun(test_weights, grad):
    #         meas_mat = np.array([forward_model.apply_single_source(src)
    #                              for src in support.transpose()])
    #         val, _ = cls.loss_and_grad(test_weights @ meas_mat - target)
    #         if grad.size > 0:
    #             grad[:] = 2*(test_weights @ meas_mat - target) @ meas_mat.transpose()
    #         return val

    #     num_srcs = support.shape[1]
    #     start_pnt = np.random.rand(num_srcs)*max_mass/num_srcs                        # non-deterministic, cf nnlasso IP
    #     #start_pnt = np.ones(num_srcs)*max_mass/num_srcs
    #     opt = nlopt.opt(nlopt.LD_MMA, num_srcs)
    #     set_nlopt_stopping_criteria(opt)
    #     opt.set_lower_bounds(np.full(num_srcs, 0))
    #     #opt.set_upper_bounds(upper_bounds)
    #     opt.set_min_objective(obj_fun)
    #     opt_res = opt.optimize(start_pnt)
    #     if opt.last_optimize_result() < 0:
    #         print("Warning (optimize_weights): optimization returned non-optimal result")
    #     return opt_res

    # @staticmethod
    # def optimize_weights(forward_model, support, target, max_mass):
    #     # res = jsfd(jmod, jloss, support, target, float(max_mass))
    #     res = jnnlasso(np.column_stack([forward_model.apply_single_source(src) for src in support.transpose()]), target, float(max_mass))
    #     return res

    @staticmethod
    def optimize_weights(forward_model, support, target, max_mass):
        return nnlasso(np.column_stack([forward_model.apply_single_source(src)
                                        for src in support.transpose()]), target, max_mass)


    # @staticmethod
    # def optimize_weights(forward_model, support, target, max_mass):
    #     meas = cvxopt.matrix([forward_model.apply_single_source(src).tolist() for src in support.transpose()])
    #     obj_mat = meas.T * meas
    #     obj_vec = -meas.T * cvxopt.matrix(target)
    #     num_srcs = support.shape[1]
    #     ineq_mat = cvxopt.sparse([cvxopt.spdiag(num_srcs*[-1]),
    #                               cvxopt.matrix(1, (1, num_srcs))])
    #     ineq_vec = cvxopt.matrix(0.0, (num_srcs+1, 1))
    #     ineq_vec[num_srcs, 0] = float(max_mass)
    #     opt_res = cvxopt.solvers.qp(obj_mat, obj_vec, ineq_mat, ineq_vec, options={'show_progress': False})
    #     if opt_res['status'] != 'optimal':
    #         print("Warning (optimize_weights): optimization returned non-optimal result")
    #     #print("weights", np.array(opt_res['x']).flatten())
    #     return np.array(opt_res['x']).flatten()