import itertools

import scipy.sparse as sp

from utils import *

class Discretization:
    def __init__(self, tau, K, num_dirs, num_rads, num_extra_ns, grid_size_mu, grid_size_nu, adaptive_grids=False):
        self.tau = tau
        self.K = K
        self.num_dirs = num_dirs
        self.num_rads = num_rads
        self.num_extra_ns = num_extra_ns
        self.grid_size_mu = grid_size_mu
        self.grid_size_nu  = grid_size_nu
        self.adaptive_grids = adaptive_grids

        self.gs_nu_sq = self.grid_size_nu**2
        self.gs_mu_sq = self.grid_size_mu**2
        self.num_time_steps = 2*self.K+1
        self.num_total_ns = self.num_extra_ns + self.num_time_steps

        self.dofs_per_var = self.num_dirs*[self.gs_mu_sq] + self.num_total_ns*[self.gs_nu_sq]
        self.total_dofs = np.sum(self.dofs_per_var)

        self.dirs = np.array([(np.cos(angle), np.sin(angle))
                              for angle in np.linspace(-np.pi/2, np.pi/2, self.num_dirs)])
        self.times = np.linspace(-self.tau*self.K, self.tau*self.K, self.num_time_steps)
        self.ns = np.empty((self.num_total_ns, 2))
        if self.num_extra_ns > 0:
            self.ns[:self.num_extra_ns] = [(np.cos(angle), np.sin(angle))
                                        for angle in np.linspace(-np.pi/2, np.pi/2, self.num_extra_ns)]
        # add n-dirs corresponding to times
        for ti, t in enumerate(self.times):
            self.ns[self.num_extra_ns+ti] = np.array((1,t))/np.sqrt(1+t**2)

        if self.adaptive_grids:
            self.rads = self.adapted_rad_grids()
            self.nu_grids = self.adapted_nu_grids()
            if self.num_dirs > 0:
                self.mu_grids = self.adapted_mu_grids()    
        else:
            # TODO: rethink rads range
            raise Exception("rads?")
            self.rads = np.linspace(-2, 2, self.num_rads)
            self.mu_grid = self.oversized_mu_grid()
            self.nu_grid = self.oversized_nu_grid()

    def oversized_mu_grid(self):
        xgrid = np.linspace(-1, np.sqrt(2), self.grid_size_mu)
        ygrid = np.linspace(-1/np.sqrt(2), 1/np.sqrt(2), self.grid_size_mu)
        return np.array(list(itertools.product(xgrid, ygrid)))

    def oversized_nu_grid(self):
        time_max = self.K*self.tau
        grid1 = np.linspace(-1/(2*time_max), max(1, 1/(2*time_max)), self.grid_size_nu)
        return np.array(list(itertools.product(grid1, grid1)))

    def adapted_rad_grids(self):
        time_max = self.K*self.tau
        rads = np.empty((self.num_dirs, self.num_total_ns, self.num_rads))
        for di, d in enumerate(self.dirs):
            for ni, n in enumerate(self.ns):
                vscal = abs(n[1])/time_max
                if n[0]-vscal >= 0:
                    if d[1] < 0:
                        left, right = (d[1]*n[0], d[0]*n[0])
                    else:
                        left, right = (0, np.sum(d)*n[0])
                else:
                    minus, plus = 0.5*(n[0]-vscal), 0.5*(n[0]+vscal)
                    if d[1] < 0:
                        left, right = (d[0]*minus+d[1]*plus, d[0]*plus+d[1]*minus)
                    else:
                        left, right = (d[0]*minus+d[1]*minus, d[0]*plus+d[1]*plus)

                rads[di, ni] = np.linspace(left, right, self.num_rads)

        return rads

    def adapted_mu_grids(self):
        # TODO: tmax?
        std_grid1 = np.linspace(0, 1, self.grid_size_mu)
        std_grid = np.array(list(itertools.product(std_grid1, std_grid1)))
        rot45 = 0.5*np.array([[1, 1], [-1, 1]])
        rot_grid = std_grid@rot45.transpose()
        res = np.empty((self.num_dirs, len(std_grid), 2))
        for di, d in enumerate(self.dirs):
            if d[1]>0:
                res[di] = rot_grid*np.sum(d)
            else:
                res[di] = rot_grid*(d[0]-d[1]) + (d[1],0)

        return res

    def adapted_nu_grid_1d(self, n):
        time_max = self.K*self.tau
        vscal = abs(n[1])/time_max
        if self.K == 0 or n[0]-vscal >= 0:
            grid_nu1 = np.linspace(0, n[0], self.grid_size_nu) 
        else:
            grid_nu1 = np.linspace(0.5*(n[0]-vscal), 0.5*(n[0]+vscal), self.grid_size_nu) 
        return grid_nu1

    def adapted_nu_grids(self):
        res = np.empty((self.num_total_ns, self.gs_nu_sq, 2))
        for ni, n in enumerate(self.ns):
            grid_nu1 = self.adapted_nu_grid_1d(n)
            res[ni] = np.array(list(itertools.product(grid_nu1, grid_nu1)))

        return res

    def build_sparse_sys_matrix(self, meas_mats, nu_radon_mats, mu_radon_mats):
        meas_block = sp.block_diag(meas_mats)

        if self.num_dirs == 0:
            return meas_block

        if self.adaptive_grids:
            # nu_radon_mats, mu_radon_mats are 2d np.arrays of sparse matrices
            # since there is a different grid and thus a different radon matrix for each nu(n,.), mu(dir,.)
            nu_radon_block = -1*sp.block_diag([sp.vstack(single_n_all_dirs)
                                            for single_n_all_dirs in nu_radon_mats])
            mu_radon_block = sp.vstack([sp.block_diag(single_n_all_dirs)
                                        for single_n_all_dirs in mu_radon_mats.transpose()])
        else:
            # nu_radon_mats, mu_radon_mats are lists of sparse matrices
            nu_radon_block = -1*sp.block_diag(self.num_total_ns*[sp.vstack(nu_radon_mats)])
            mu_radon_block = sp.vstack([sp.block_diag(self.num_dirs*[single_n])
                                        for single_n in mu_radon_mats])

        row0 = sp.hstack([mu_radon_block, nu_radon_block])
        zeros_bl = sp.coo_matrix((meas_block.shape[0], row0.shape[1] - meas_block.shape[1]))
        row1 = sp.hstack([zeros_bl, meas_block])
        Msys = sp.vstack([row0, row1])

        return Msys

    def build_main_objective(self, model, target):
        if self.adaptive_grids:
            # build matrices for line projections of mu
            # for each d in dirs, mu(d, .) is projected onto the direction of each n in ns
            mu_radon_mats = np.empty((self.num_dirs, self.num_total_ns), dtype=object)
            for di in range(self.num_dirs):
                print("Building mu radon mats: {}/{}".format(di+1, self.num_dirs))
                mu_radon_mats[di] = build_radon_matrix(self.mu_grids[di], self.ns, self.rads[di])

            # build matrices for line projections of nu
            # for each n in ns, nu(n, .) is projected onto the direction of each d in dirs
            nu_radon_mats = np.empty((self.num_total_ns, self.num_dirs), dtype=object)
            if self.num_dirs > 0:
                for ni, n in enumerate(self.ns):
                    print("Building nu radon mats: {}/{}".format(ni+1, self.num_total_ns))
                    nu_radon_mats[ni] = build_radon_matrix(self.nu_grids[ni], self.dirs, self.rads[:,ni])
                
            # build measurement matrix for each time step by applying forward model to each grid point
            # the grid points for nu need to be scaled since u_k = [x -> sqrt(1+(k*tau)^2)*x]_# nu(n_k, .)
            meas_mats = np.empty((self.num_time_steps, model.meas_size(), self.gs_nu_sq))
            for ti, t in enumerate(self.times):
                nu_grid_for_time = self.nu_grids[-self.num_time_steps+ti]
                for j, pnt in enumerate(nu_grid_for_time):
                    tpnt = pnt*np.sqrt(1+t**2)
                    meas_mats[ti,:,j] = model.apply_single_source(tpnt)

        else: # no adaptive grids
            print("Building radon matrices for mu...")
            mu_radon_mats = build_radon_matrix(self.mu_grid, self.ns, self.rads)
            print("Building radon matrices for nu...")
            nu_radon_mats = build_radon_matrix(self.nu_grid, self.dirs, self.rads)

            print("Building measurement matrices...")
            meas_mats = np.zeros((self.num_time_steps, model.meas_size(), self.gs_nu_sq))
            for ti, t in enumerate(self.times):
                for j, pnt in enumerate(self.nu_grid):
                    tpnt = pnt*np.sqrt(1+t**2)
                    # TODO: think about this
                    if np.all(0<=tpnt) and np.all(tpnt<=1):
                        meas_mats[ti,:,j] = model.apply_single_source(tpnt)

        print("Building Msys...")
        Msys = self.build_sparse_sys_matrix(meas_mats, nu_radon_mats, mu_radon_mats)
        y = np.hstack((np.zeros(Msys.shape[0]-target.shape[0]), target))

        nonzero = Msys.count_nonzero()
        percent_nz = nonzero/np.prod(Msys.shape)*100
        print('Msys shape = {}, nonzero = {} ({:.3}%)'.format(Msys.shape, nonzero, percent_nz))

        return Msys, y

    def build_nonneg_sum_constr(self, l1_target):
        nonneg_mat = -1*sp.eye(self.total_dofs)
        sum_mat = sp.block_diag([dofs*[1] for dofs in self.dofs_per_var])
        constr_mat = sp.vstack([nonneg_mat,
                                sum_mat])
        rhs = np.hstack((np.zeros(self.total_dofs),
                         np.full(len(self.dofs_per_var), l1_target)))

        return constr_mat, rhs

    def build_l1_constr(self, l1_target):
        # need one additional var |x| for each dof x which represents absolute value of that dof
        # use constraints -|x| <= x <= |x|
        # block of new abs vars is ordered in front of real vars
        data = np.full(2*self.total_dofs, -1.0)
        I = np.arange(2*self.total_dofs)
        J = np.repeat(np.arange(self.total_dofs), 2)
        abs_part = sp.coo_matrix((data, (I, J)))

        data = np.tile([1.0, -1.0], self.total_dofs)
        I = np.arange(2*self.total_dofs)
        J = np.repeat(np.arange(self.total_dofs), 2)
        dof_part = sp.coo_matrix((data, (I, J)))

        # sum up absolute values for each var
        sum_mat = sp.block_diag([dofs*[1.0] for dofs in self.dofs_per_var])
        constr_mat = sp.bmat([[abs_part, dof_part],
                              [sum_mat, None]]) # zeros in bottom right because vars without abs are not summed

        rhs = np.hstack((np.zeros(2*self.total_dofs),                 # zeros for abs ineqs
                         np.full(len(self.dofs_per_var), l1_target))) # sum to l1_target
        
        return constr_mat, rhs

    def build_vel_objective(self, mu):
        std_grid1 = np.linspace(0, 1, self.grid_size_nu)
        std_grid = np.array(list(itertools.product(std_grid1, std_grid1)))

        radon_mat = build_radon_matrix(std_grid, self.dirs, self.rads)

        # TODO: Vmat should be sparse
        Vmat = np.empty((self.num_dirs*self.num_rads, 2*len(std_grid)))
        for di, d in enumerate(self.dirs):
            Vmat[di*self.num_rads:(di+1)*self.num_rads,:len(std_grid)] = d[0]*radon_mat[di].todense()
            Vmat[di*self.num_rads:(di+1)*self.num_rads,len(std_grid):] = d[1]*radon_mat[di].todense()

        if self.adaptive_grids:
            mu_radons = np.zeros((1, self.num_dirs, self.num_rads, self.gs_mu_sq))
            for di, d in enumerate(self.dirs):
                mu_radons[:,di,:,:] = build_radon_matrix(self.mu_grids[di], [[1,0]], self.rads)

            mumat = sp.block_diag([mu_radons[0,di]*self.mu_grids[di,:,1] for di in range(self.num_dirs)])
        else:
            mu_radon = build_radon_matrix(self.mu_grid, [[1,0]], self.rads)[0]
            mumat = sp.block_diag(self.num_dirs*[mu_radon.multiply(self.mu_grid[:,1])])

        print(mumat.shape, mu.flatten().shape)

        b = mumat@mu.flatten()

        return Vmat, b

    def unpack_sol_vec(self, sol):
        if self.num_dirs > 0:
            mu = sol[:self.num_dirs*self.gs_mu_sq].reshape(self.num_dirs, self.grid_size_mu, -1)
        else:
            mu = None
        nu = sol[self.num_dirs*self.gs_mu_sq:].reshape(self.num_total_ns, self.grid_size_nu, -1)
        return mu, nu