#!/bin/bash
 
#SBATCH --job-name=master-thesis         # the name of your job
#SBATCH --output=output_%a.dat      # the file where output is written to (stdout/stderr)

#SBATCH --partition=normal          # on which partition to submit the job
#SBATCH --time=00:10:00             # the max wallclock time (time limit your job will run)

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --mem-per-cpu=1G

#SBATCH --array=1-1000

# can't run file in res dir because imports don't work
srun python ~/code/master-wirth/palma-script.py discn5 $SLURM_ARRAY_TASK_ID