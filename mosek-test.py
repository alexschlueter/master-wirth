from mosek import *

# M = Model()
# t = M.variable("t")
# x = M.variable("x")
# for i in range(len(dirs)):
#     # mu = M.variable("mu"+i, grid_size_mu**2, Domain.greaterThan(0))
#     M.constraint(Expr.sum(x, ), Domain.lessThan(weights.sum()))
# for i in range(len(ns)):
#     nu = M.variable("nu"+i, grid_size**2, Domain.greaterThan(0))
# expr = x.index(0)
# M.constraint(Expr.vstack(t))
A = cvxopt.sparse([cvxopt.spmatrix(0.0, (1, len(dirs)+len(ns))), ineq_mat[Msys.shape[1]:,:]
res = np.array()
with Env() as env:
    with env.Task(0, 0) as task:
        task.set_Stream(mosek.streamtype.log, streamprinter)
        task.appendvars(1+Msys.shape[1])
        task.appendcons(len(dirs)+len(ns))
        task.putcj(0, 1.0)
        for i in range(Msys.shape[1])
            task.putvarbound(1+i, mosek.boundkey.lo, 0.0)
        # for k in range(len(dirs)):
        #     task.putarow(i, )
        task.putaijlist(A.I, A.J, A.V)
        for i in range(len(dirs)+len(ns)):
            task.putconbound(i, mosek.boundkey.up, -inf, weights.sum())
        task.appendconeseq(mosek.conetype.quad, 0.0, +inf)
        task.putobjsense(objsense.minimize)
        task.optimize()
        task.getxx(soltype.itr, res)