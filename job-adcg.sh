#!/bin/bash
 
#SBATCH --job-name=master-thesis         # the name of your job
#SBATCH --output=output_%a.dat      # the file where output is written to (stdout/stderr)

#SBATCH --partition=normal          # on which partition to submit the job
#SBATCH --time=04:00:00             # the max wallclock time (time limit your job will run)

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=500M

#SBATCH --array=1-32

. /home/a/a_schl57/load-modules.sh
export PYTHONPATH="/home/a/a_schl57/code/master-wirth:$PYTHONPATH"
srun julia ./palma-adcg-miss2.jl $SLURM_ARRAY_TASK_ID