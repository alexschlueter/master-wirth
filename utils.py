import time
from pathlib import Path
from datetime import datetime
from operator import itemgetter
from bisect import bisect_left, bisect_right

from matplotlib import transforms
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as sp
from scipy import ndimage
from scipy import spatial
from sklearn.neighbors import NearestNeighbors

def build_radon_matrix(grid, dirs, rads):
    dots = dirs@grid.transpose()
    # proj_inds = np.searchsorted(rads, dots)
    # weights_left = (rads[proj_inds]-dots) / rad_bin_len
    # dd, dg = np.ogrid[0:len(dirs),0:len(grid)]
    # res[dd, proj_inds-1, dg] = weights_left
    # res[dd, proj_inds, dg] = 1-weights_left
    res = []
    for di, d in enumerate(dirs):
        proj_inds = np.searchsorted(rads[di], dots[di])
        num_rads = len(rads[di])
        right = (proj_inds == num_rads)
        left = (proj_inds == 0)
        mid = ~(right | left)
        left_grid_idc = np.flatnonzero(left)
        right_grid_idc = np.flatnonzero(right)
        mid_grid_idc = np.flatnonzero(mid)
        num_left = len(left_grid_idc)
        num_right = len(right_grid_idc)
        if num_left > 0 and np.max(rads[di][0]-dots[di][left])>1e-7:
            print("lol: ", num_left, np.max(rads[di][0]-dots[di][left]))
        if num_right > 0 and np.max(dots[di][right]-rads[di][99])>1e-7:
            print("lor: ", num_right, np.max(dots[di][right]-rads[di][99]))
        proj_inds_mid = proj_inds[mid]
        dots_mid = dots[di][mid]
        rad_bin_len = rads[di][1]-rads[di][0]
        weights_left = (rads[di][proj_inds_mid]-dots_mid) / rad_bin_len
        dir_mat = sp.coo_matrix((weights_left, (proj_inds_mid-1, mid_grid_idc)), (num_rads, len(grid)))
        dir_mat += sp.coo_matrix((1-weights_left, (proj_inds_mid, mid_grid_idc)), (num_rads, len(grid)))
        dir_mat += sp.coo_matrix((np.ones_like(left_grid_idc), (np.zeros_like(left_grid_idc), left_grid_idc)), (num_rads, len(grid)))
        dir_mat += sp.coo_matrix((np.ones_like(right_grid_idc), (np.full_like(right_grid_idc, num_rads-1), right_grid_idc)), (num_rads, len(grid)))
        # dir_mat = sp.coo_matrix((weights_left[di], (proj_inds[di]-1, np.arange(len(grid)))), (len(rads), len(grid)))
        # dir_mat += sp.coo_matrix((1-weights_left[di], (proj_inds[di], np.arange(len(grid)))), (len(rads), len(grid)))
        res.append(dir_mat)
    return res

def announce_save(descr, prefix, arr, folder=".", with_time=False):
    fname = folder+"/"+prefix
    if with_time:
        fname += "_"+time.strftime("%Y%m%d-%H%M%S")
    print("Saving {} to {}.npy".format(descr, fname)) 
    np.save(fname, arr)

def get_timed_files(folder, prefix):
    paths = list(Path(folder).glob(prefix+'*.npy'))
    dts = [
        datetime.strptime(path.name, prefix+"_%Y%m%d-%H%M%S.npy")
        for path in paths
    ]
    return list(zip(*sorted(zip(paths, dts), key=itemgetter(1))))

def list_of_sources(img, zero_threshold=1e-3):
    img[img<zero_threshold] = 0
    labelled, num_sources = ndimage.label(img, structure=np.ones((3,3)))
    com = np.array(ndimage.center_of_mass(img, labelled, 1+np.arange(num_sources)))
    # normalize 0..1
    assert img.shape[0]==img.shape[1]
    com /= img.shape[0]-1
    # com /= disc.grid_size_nu-1
    # com *= right-left
    # com += left
    # print(com)
    return com
    # g = np.empty((disc.grid_size_nu, disc.grid_size_nu), dtype=object)
    # g[:] = disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2)
    # # print(disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2))
    # return ndimage.interpolation.map_coordinates(disc.nu_grid.reshape(disc.grid_size_nu, disc.grid_size_nu, 2), com.transpose())

def load_res_for_time(files, time):
    idx = bisect_right(files[1], time)
    if idx == len(files[1]):
        return None
    res_file = files[0][idx]
    # print(gt_file, files[discstr][0][idx])
    return np.load(res_file, allow_pickle=True)

def match_sources(gt, res, radius=0.01):
    nbrs = NearestNeighbors().fit(gt)
    # dists, inds = nbrs.kneighbors(los, n_neighbors=1)
    dists, inds = nbrs.radius_neighbors(res, radius=radius)
    # TODO: not sorted by distance!
    matched = len(gt)*[False]
    true_pos = 0
    for srcinds in inds:
        for ind in srcinds:
            if not matched[ind]:
                matched[ind] = True
                true_pos += 1 
                break
    return true_pos

def torus_dist(v, w):
    return np.max(np.minimum(np.abs(v-w), 1-np.abs(v-w)))

def min_torus_sep(sources):
    return np.min(spatial.distance.pdist(sources, torus_dist))

def min_dyn_sep(support, K, tau):
    min_seps = []
    for k in range(-K, K+1):
        min_seps.append(min_torus_sep(support[:,:2]+k*tau*support[:,2:]))
    return np.min(min_seps)

def extent_from_gs(gs):
    real_x = real_y = np.linspace(0,1,gs)
    dx = (real_x[1]-real_x[0])/2.
    dy = (real_y[1]-real_y[0])/2.
    return [real_x[0]-dx, real_x[-1]+dx, real_y[0]-dy, real_y[-1]+dy]

def mu_transform(theta):
    if theta[1]>0:
        return np.sum(theta)/np.sqrt(2), (0,0)
    else:
        return (theta[0]-theta[1])/np.sqrt(2), (theta[1], 0)

def mu_mpl_transform(theta):
    scal, off = mu_transform(theta)
    return transforms.Affine2D().rotate_deg(-45).scale(scal).translate(*off)

def plot_mu_sol(ax, disc, mu, theta_idx, **kwargs):
    tr = mu_mpl_transform(disc.dirs[theta_idx])
    extent = extent_from_gs(disc.grid_size_mu)
    bbox = transforms.Bbox.from_extents(np.array(extent).reshape(2,-1).transpose().flatten())
    trbox = transforms.Bbox.null()
    trbox.update_from_data_xy(tr.transform(bbox.corners()))

    img = ax.imshow(mu.transpose(), interpolation="none",
                    origin="lower", extent=extent,
                    transform=tr+ax.transData, **kwargs)

    ax.set_xlim(trbox.x0, trbox.x1)
    ax.set_ylim(trbox.y0, trbox.y1)

    return img

def plot_jrad(ax, disc, actual_sources, theta_idx, **kwargs):
    theta = disc.dirs[theta_idx]
    for s in actual_sources:
        jrad = [theta@s[:2], theta@s[2:]]
        ax.plot([jrad[0]], [jrad[1]], **kwargs)

def plot_mu_gt(*args, **kwargs):
    plot_jrad(*args, **kwargs, marker="o", fillstyle="none")

def plot_mu_los(*args, **kwargs):
    plot_jrad(*args, **kwargs, marker="x")

# def plot_nu(nu, ns)
#     for n in ns
