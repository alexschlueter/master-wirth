import os
import re
import numpy as np

missing = []
its = []
for i in range(1, 1001):
    if not os.path.isfile("./res_{}.npy".format(i)):
        missing.append(i)
        op = open("output_{}.dat".format(i), "r")
        for line in reversed(list(op)):
            l = line.rstrip()
            m = re.fullmatch(".+ iteration inside ADCG: (\d+)", l)
            if m:
                print(i, m.group(1))
                its.append(int(m.group(1)))
                break

print(len(its), min(its), max(its))
print(np.histogram(its))
np.save("missing3.npy", missing)
print(len(missing))