import itertools
import time

import cvxopt
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from datetime import datetime
from operator import itemgetter
from bisect import bisect_left, bisect_right
from scipy import ndimage
from sklearn.neighbors import NearestNeighbors

import l1regls
from models.dynamic import DynamicSuperresModel
from models.fourier import Fourier1DModel, FourierNDModel
from models.identity import IdentityModel
from discretization import *
from utils import *
from solvers import *
# from solvers.l1separable import L1SeparableConeSolver


zero_threshold = 1e-3

# np.set_printoptions(threshold=np.inf)

discs = ["full", "comb", "three", "single"]
files = dict()
for discstr in discs:
    files[discstr] = get_timed_files("data-home/", "res_"+discstr)
files["gt"] = get_timed_files("data-home/", "gt")

rads = np.linspace(0, 0.5, 1000)
rad = 0.01


# @profile
def main():
    disc_single = Discretization(
        tau = 1,
        K = 0,
        num_dirs = 0,
        num_rads = 0,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 0,
        adaptive_grids=True
    )

    disc3rad100 = Discretization(
        tau = 1,
        K = 1,
        # num_dirs = 3,
        # num_rads = 3,
        # grid_size_nu = 3,
        # grid_size_mu = 3,
        # num_extra_ns = 3,

        # num_extra_ns = 11,
        # num_rads = 11,

        # grid_size_nu = 60,
        # grid_size_mu = 60,

        # num_dirs = 17,
        # num_rads = 17,
        # num_extra_ns = 17,
        # grid_size_nu = 20,
        # grid_size_mu = 20,

        # grid_size_nu = 17,
        # grid_size_mu = 17,

        num_dirs = 30,
        # num_rads = 30,
        num_rads = 100,
        # num_extra_ns = 30,
        num_extra_ns = 0,
        grid_size_nu = 100,
        grid_size_mu = 100,
        adaptive_grids=True
    )
    # disc3 = Discretization(
    #     tau = 1,
    #     K = 1,
    #     # num_dirs = 3,
    #     # num_rads = 3,
    #     # grid_size_nu = 3,
    #     # grid_size_mu = 3,
    #     # num_extra_ns = 3,

    #     # num_extra_ns = 11,
    #     # num_rads = 11,

    #     # grid_size_nu = 60,
    #     # grid_size_mu = 60,

    #     # num_dirs = 17,
    #     # num_rads = 17,
    #     # num_extra_ns = 17,
    #     # grid_size_nu = 20,
    #     # grid_size_mu = 20,

    #     # grid_size_nu = 17,
    #     # grid_size_mu = 17,

    #     num_dirs = 30,
    #     num_rads = 30,
    #     # num_extra_ns = 30,
    #     num_extra_ns = 0,
    #     grid_size_nu = 100,
    #     grid_size_mu = 100,
    #     adaptive_grids=True
    # )

    # discfull = Discretization(
    #     tau = 1,
    #     K = 1,
    #     # num_dirs = 3,
    #     # num_rads = 3,
    #     # grid_size_nu = 3,
    #     # grid_size_mu = 3,
    #     # num_extra_ns = 3,

    #     # num_extra_ns = 11,
    #     # num_rads = 11,

    #     # grid_size_nu = 60,
    #     # grid_size_mu = 60,

    #     # num_dirs = 17,
    #     # num_rads = 17,
    #     # num_extra_ns = 17,
    #     # grid_size_nu = 20,
    #     # grid_size_mu = 20,

    #     # grid_size_nu = 17,
    #     # grid_size_mu = 17,

    #     num_dirs = 30,
    #     num_rads = 30,
    #     num_extra_ns = 30,
    #     grid_size_nu = 100,
    #     grid_size_mu = 100,
    #     adaptive_grids=True
    # )


    disc = disc3rad100
    start_time = datetime(2019, 9, 23, 0, 50, 45)
    tot_tp = 0
    tot_pos = 0
    tot_srces = 0
    for gt_file, gt_date in filter(lambda gt: gt[1]>=start_time, zip(*files["gt"])):
        _, actual_sources, weights = np.load(gt_file, allow_pickle=True)
        actual_sources = actual_sources.transpose()

        # model_static = FourierNDModel([np.arange(-2, 3), np.arange(-2, 3)], [1, 1])
        # model_static = FourierNDModel([np.arange(-10, 11), np.arange(-10, 11)], [1, 1])
        fc = 5
        model_static = FourierNDModel([np.arange(-fc, fc+1), np.arange(-fc, fc+1)], [1, 1])
        # model_static = IdentityModel([1, 1], 2*[p.grid_size_nu])

        model = DynamicSuperresModel(model_static, 1, disc.tau, disc.K)
        target = model.apply(actual_sources.transpose(), weights)

        print("Stage 1: Main objective")
        Msys, y = disc.build_main_objective(model_static, target)

        l1_target = np.linalg.norm(weights, ord=1)
        # solver = L1SeparableConeSolver(disc, Msys, y, 5.0)
        solver = SeparableConeSolver(disc, Msys, y, l1_target, nonneg=False)
        # solver = CVXQuadraticProgramSolver(disc, Msys, y, l1_target, sparse=True, kktsolver=True)

        print("Start opt...")
        start = time.time()
        mu, nu = solver.solve()
        end = time.time()
        print(end-start)
        print("Finished opt...")
        # res[res<1e-6]=0

        announce_save("mu, nu", "res_disc3rad100_signed", (mu, nu))

        los = list_of_sources(nu[-2])
        # los = list_of_sources(nu[0])
        tp = match_sources(actual_sources[:,:2], los)
        prec = tp/len(los)
        rec = tp/len(actual_sources)
        f1 = 2*prec*rec/(prec+rec)
        tot_pos += len(los)
        tot_srces += len(actual_sources)
        tot_tp += tp
        tot_prec = tot_tp/tot_pos
        tot_rec = tot_tp/tot_srces
        tot_f1 = 2*tot_prec*tot_rec/(tot_prec+tot_rec)
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("g_date={}".format(gt_date))
        print("Single \tprec={:.3f}\trec={:.3f}\tf1={:.3f}".format(prec,rec,f1))
        print("Total \tprec={:.3f}\trec={:.3f}\tf1={:.3f}".format(tot_prec,tot_rec,tot_f1))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

main()

